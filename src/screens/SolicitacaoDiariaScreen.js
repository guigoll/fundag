import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Section } from '../components/section';
import { Form, InputField, SelectField } from '../components/form';
import { DataGrid } from '../components/grid';

const SolicitacaoDiariaScreen = props => ({
    render() {
        const { user } = this.props;
        return (
            <div>
                <Section titulo="Solicitação de Diária" subtitulo="Lista de Solicitações de Diária">
                    <DataGrid />
                </Section>

                <Section titulo="Solicitação de Diária" subtitulo="Cadastro de Solicitação de Diária">
                    <hr />
                    <Form onSubmit={this.onSubmit}>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <label>Projeto</label>
                                        <InputField
                                            type="text"
                                            name="Projeto"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Unidade Contratante</label>
                                        <InputField
                                            type="text"
                                            name="unidContratante"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Unidade Realizadora</label>
                                        <InputField
                                            type="text"
                                            name="unidRealizadora"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Unidade Operacional</label>
                                        <InputField
                                            type="text"
                                            name="unidOperacional"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Programa</label>
                                        <InputField
                                            type="text"
                                            name="programa"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Titulo</label>
                                        <InputField
                                            type="text"
                                            name="titulo"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Objetivos</label>
                                        <InputField
                                            type="text"
                                            name="objetivo"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <label>Tipo Instituição</label>
                                        <InputField
                                            type="text"
                                            name="tipoInstituicao"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Cordenador</label>
                                        <InputField
                                            type="text"
                                            name="cordenador"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Pesquisador</label>
                                        <InputField
                                            type="text"
                                            name="pesquisador"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Código Projeto</label>
                                        <InputField
                                            type="text"
                                            name="Titulo"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <button type="button" class="btn btn-primary">Salvar</button>&nbsp;&nbsp;
                                            <button type="button" class="btn btn-default">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                </Section>
            </div>
        );
    }
});

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    };
}

export default connect(mapStateToProps)(SolicitacaoDiariaScreen)

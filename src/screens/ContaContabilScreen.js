import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Section } from '../components/section';
import { Form, InputField, SelectField } from '../components/form';
import { DataGrid } from '../components/grid';

class ContaContabilScreen extends Component {
    constructor() {
        super();
        this.state = {
            gridVisible: true,
        }
    }

    alterarEstado() {
        var Visible;
        if (this.state.gridVisible) {
            Visible = false
        } else {
            Visible = true
        }
        this.setState({
            gridVisible: Visible
        })
    }

    render() {
        const { gridVisible } = this.state;
        const data = [{
            codigo: 1,
            projeto: 'Projeto A',
            unidContratante: 'Unidade Contratante A'
        }, {
            codigo: 1,
            projeto: 'Projeto B',
            unidContratante: 'Unidade Contratante B'
        }, {
            codigo: 1,
            projeto: 'Projeto C',
            unidContratante: 'Unidade Contratante C'
        }, {
            codigo: 1,
            projeto: 'Projeto D',
            unidContratante: 'Unidade Contratante D'
        }, {
            codigo: 1,
            projeto: 'Projeto E',
            unidContratante: 'Unidade Contratante E'
        }]

        const columns = [{
            Header: 'Cód.',
            accessor: 'codigo',
            maxWidth: 100
        }, {
            Header: 'Projeto',
            accessor: 'projeto',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Unidade Contratante',
            accessor: 'unidContratante',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }]

        const { user } = this.props;
        return (
            <div>
                { gridVisible ?
                <Section titulo="Conta Contábil" subtitulo="Lista de Contas Contábeis">
                    <div className="panel-content">
                        <button onClick={this.alterarEstado.bind(this)} className="btn btn-primary">Novo</button>
                    </div>
                    <DataGrid columns={columns} data={data} />
                </Section>
                :

                <Section titulo="Conta Contábil" subtitulo="Cadastro de Conta Contábil">
                    <Form onSubmit={this.onSubmit}>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <label>Projeto</label>
                                        <InputField
                                            type="text"
                                            name="Projeto"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Unidade Contratante</label>
                                        <SelectField
                                            value="1"
                                            className="form-control"
                                            options={['Unidade 1', 'Unidade 2']}
                                            valor="uma unidade contratante"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                </div>

                            </div>
                            <div className="col-md-6">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <label>Código Projeto</label>
                                        <InputField
                                            type="text"
                                            name="Titulo"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <button type="button" onClick={this.alterarEstado.bind(this)} className="btn btn-primary">Salvar</button>&nbsp;&nbsp;
                                            <button type="button" onClick={this.alterarEstado.bind(this)} className="btn btn-default">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                </Section>
                }
            </div>
        );
    }
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    };
}

export default connect(mapStateToProps)(ContaContabilScreen)

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Section } from '../components/section';
import { Form, InputField, SelectField } from '../components/form';
import { DataGrid } from '../components/grid';

class ExtratoScreen extends Component {

    constructor() {
        super();
        this.state = {
            descFiltro: "Últimos Lançamentos",
            alterDesc: true
        }
    }

    alterarDesc() {
        var desc;
        var alt;
        if (this.state.alterDesc) {               
            desc= "Lançamentos do filtro" ;
            alt= false;        
        } else {
            desc= "Últimos Lançamentos" ;
            alt= true ;            
        }
        this.setState({
            descFiltro: desc, 
            alterDesc: alt           
        })
    }
    render() {
        const desc = this.state.descFiltro
        const data = [{
            data: '13/04/2018',
            valor: '100,00',
            historico: 'Pag. Nota Fiscal',
            debito: '',
            credito: '10000.00',
            saldo: '1000.00'
        }, {
            data: '13/04/2018',
            valor: '250,00',
            historico: 'Despesas KM',
            debito: '200.00',
            credito: '',
            saldo: '800.00'
        }, {
            data: '13/04/2018',
            valor: '100,00',
            historico: 'Despesas Diária',
            debito: '100.00',
            credito: '',
            saldo: '900.00'
        }]

        const columns = [{
            Header: 'Data',
            accessor: 'data',
            maxWidth: 100
        }, {
            Header: 'Valor Doc.',
            accessor: 'valor',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Historico Padrão',
            accessor: 'historico',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Debito',
            accessor: 'debito',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Crédito',
            accessor: 'credito',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Saldo',
            accessor: 'saldo',
            Cell: props => <span className='text-left'>{props.value}</span>
        }]

        return (
            <div>
                <Section titulo="Extratos" subtitulo="Filtrar extrato">
                    <div className="panel-content">
                        <Form onSubmit={this.onSubmit}>
                            <div className="form-group col-md-3">
                                <SelectField
                                    value="1"
                                    className="form-control"
                                    valor="um projeto"
                                    options={['Projeto A', 'Projeto B']}
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="form-group col-md-3">
                                <InputField
                                    type="text"
                                    name="dataInicio"
                                    className="form-control"
                                    placeholder="Data Inicial"
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="form-group col-md-3">
                                <InputField
                                    type="text"
                                    name="dataFim"
                                    className="form-control"
                                    placeholder="Data Final"
                                    onChange={this.onChange}
                                />
                            </div>                            
                        </Form>
                        <button onClick={this.alterarDesc.bind(this)} className="btn btn-primary">Filtrar</button>
                    </div>
                </Section>

                <Section subtitulo={desc}>
                    <div className="row">
                        <div className="col-md-3 col-xs-offset-6">
                            <div className="form-group">
                                <label>Saldo Inicial</label>
                                <InputField
                                    type="text"
                                    value="2000,00"
                                    name="saldoInicial"
                                    className="form-control col-md-2 disabled"
                                    onChange={this.onChange}
                                />
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-group">
                                <label>Saldo Final</label>
                                <InputField
                                    type="text"
                                    name="saldoFinal"
                                    value="1000,00"
                                    className="form-control col-md-2 disabled"
                                    onChange={this.onChange}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="panel-content">
                            <DataGrid columns={columns} data={data} />
                        </div>
                    </div>
                </Section>
            </div >
        );
    }
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    };
}

export default connect(mapStateToProps)(ExtratoScreen)

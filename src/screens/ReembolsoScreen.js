import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { Section } from '../components/section';
import { InputField } from '../components/form';
import { DataGridAprova } from '../components/grid';
import { ButtonDropdow } from '../components/button';

const ReembolsoSeleciona = () => ({
   render()
   {
     const columnsContinuar = [{
         Header: 'Tipo',
         headerClassName: 'text-left',
         accessor: 'tipo'
     }, {
         Header: 'Código',
         accessor: 'codigo',
         headerClassName: 'text-left',
         Cell: props => <span>{props.value}</span>
     }, {
         Header: 'Projeto',
         headerClassName: 'text-left',
         accessor: 'projeto',
         Cell: props => <span className='text-left'>{props.value}</span>
     }, {
         Header: 'Nome',
         headerClassName: 'text-left',
         accessor: 'nome',
         Cell: props => <span className='text-left'>{props.value}</span>
     }, {
         Header: 'Criticidade',
         accessor: 'criticidade',
         Cell: props => <span className='text-left'>{props.value}</span>
     }, {
         Header: 'Data',
         headerClassName: 'text-left',
         accessor: 'data',
         Cell: props => <span className='text-right'>{props.value}</span>
     }, {
         Header: 'Valor',
         headerClassName: 'text-left',
         accessor: 'valor',
         Cell: props => <span className='text-right'>{props.value}</span>
     }, {
         Header: '',
         accessor: 'edicao',
         Cell: props => [
             <InputField
                 type="checkbox"
                 className="fancy-checkbox"
             />
         ]
     }]

     const data = [{
         tipo: "Adiantamento",
         codigo: 'AD-0001',
         projeto: 'projeto A',
         nome: 'Guilherme',
         criticidade: 'baixa',
         data: '05/09/2018',
         valor: 'R$ 50,00'
     }, {
         tipo: "Adiantamento",
         codigo: 'AD-0002',
         projeto: 'projeto A',
         nome: 'Guilherme',
         criticidade: 'baixa',
         data: '05/09/2018',
         valor: 'R$ 50,00'
     }, {
         tipo: "Prestação de Conta",
         codigo: 'AD-0003',
         projeto: 'projeto A',
         nome: 'Guilherme',
         criticidade: 'baixa',
         data: '05/09/2018',
         valor: 'R$ 50,00'
     }, {
         tipo: "Bolsa",
         codigo: 'AD-0004',
         projeto: 'projeto B',
         nome: 'Leonardo',
         criticidade: 'baixa',
         data: '05/09/2018',
         valor: 'R$ 50,00'
     }, {
         tipo: "Prestação de Conta",
         codigo: 'AD-0005',
         projeto: 'projeto C',
         nome: 'Marcelo',
         criticidade: 'alta',
         data: '05/09/2018',
         valor: 'R$ 60,00'
     }, {
         tipo: "Adiantamento",
         codigo: 'AD-0006',
         projeto: 'projeto A',
         nome: 'Guilherme',
         criticidade: 'baixa',
         data: '05/09/2018',
         valor: 'R$ 50,00'
     },]

     return(
       <Section titulo="Reembolso"
             subtitulo="você possui adiantamentos pendentes é necessario vincular para continuar">
             <div className="panel-content row">
                 <Link to="/reembolsogenerico" className="btn btn-warning pull-right">Continuar</Link>
             </div>
             <DataGridAprova columns={columnsContinuar} data={data} />
         </Section>);
   }
});

class ReembolsoScreen extends Component {

    constructor() {
        super();
        this.state = {
            gridVisible: true,
        }
    }

    alterarEstado() {
        var Visible;
        if (this.state.gridVisible) {
            Visible = false
        } else {
            Visible = true
        }
        this.setState({
            gridVisible: Visible
        })
    }
    render() {
        const { gridVisible } = this.state;
        const data = [{
            tipo: "Adiantamento",
            codigo: 'AD-0001',
            projeto: 'projeto A',
            nome: 'Guilherme',
            criticidade: 'baixa',
            data: '05/09/2018',
            valor: 'R$ 50,00'
        }, {
            tipo: "Adiantamento",
            codigo: 'AD-0002',
            projeto: 'projeto A',
            nome: 'Guilherme',
            criticidade: 'baixa',
            data: '05/09/2018',
            valor: 'R$ 50,00'
        }, {
            tipo: "Prestação de Conta",
            codigo: 'AD-0003',
            projeto: 'projeto A',
            nome: 'Guilherme',
            criticidade: 'baixa',
            data: '05/09/2018',
            valor: 'R$ 50,00'
        }, {
            tipo: "Bolsa",
            codigo: 'AD-0004',
            projeto: 'projeto B',
            nome: 'Leonardo',
            criticidade: 'baixa',
            data: '05/09/2018',
            valor: 'R$ 50,00'
        }, {
            tipo: "Prestação de Conta",
            codigo: 'AD-0005',
            projeto: 'projeto C',
            nome: 'Marcelo',
            criticidade: 'alta',
            data: '05/09/2018',
            valor: 'R$ 60,00'
        }, {
            tipo: "Adiantamento",
            codigo: 'AD-0006',
            projeto: 'projeto A',
            nome: 'Guilherme',
            criticidade: 'baixa',
            data: '05/09/2018',
            valor: 'R$ 50,00'
        },]

        const columns = [{
            Header: 'Tipo',
            headerClassName: 'text-left',
            accessor: 'tipo'
        }, {
            Header: 'Código',
            accessor: 'codigo',
            headerClassName: 'text-left',
            Cell: props => <span>{props.value}</span>
        }, {
            Header: 'Projeto',
            headerClassName: 'text-left',
            accessor: 'projeto',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Nome',
            headerClassName: 'text-left',
            accessor: 'nome',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Criticidade',
            accessor: 'criticidade',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Data',
            headerClassName: 'text-left',
            accessor: 'data',
            Cell: props => <span className='text-right'>{props.value}</span>
        }, {
            Header: 'Valor',
            headerClassName: 'text-left',
            accessor: 'valor',
            Cell: props => <span className='text-right'>{props.value}</span>
        }, {
            Header: '',
            accessor: 'edicao',
            Cell: props => [
                <ButtonDropdow options={[
                    { description: 'visualizar' },
                    { description: 'continuar' }
                ]} />
            ]
        }]
        return (
            <div>
                {gridVisible ?
                    <Section titulo="Reembolso"
                        subtitulo="Continuar não enviados">
                        <div className="panel-content">
                            <button onClick={this.alterarEstado.bind(this)} className="btn btn-primary">Novo</button>
                        </div>
                        <DataGridAprova columns={columns} data={data} />
                    </Section>
                    :
                    <ReembolsoSeleciona />
                }

            </div >
        );
    }
};

export default ReembolsoScreen;

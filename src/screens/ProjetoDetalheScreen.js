import React, { Componet } from 'react';

import { Section } from '../components/section';
import { Form, InputField, SelectField } from '../components/form';
import { DataGridPlanoDeTrebalho } from '../components/grid';



const ProjetoDetalheScreen = () => ({
    render() {
        return (
            <div>
                <Section titulo="Projetos"
                    icon="lnr lnr-magic-wand"
                    subtitulo="Detalhes de Projetos">
                    <Form onSubmit={this.onSubmit}>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-2">
                                                <label>Código</label>
                                                <InputField
                                                    type="text"
                                                    name="codigo"                                                 
                                                    className="form-control disabled"
                                                    onChange={this.onChange}
                                                />
                                            </div> <div className="col-md-10">
                                                <label>Titulo</label>
                                                <InputField
                                                    type="text"
                                                    name="titulo"
                                                    className="form-control disabled"
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label>Unidade Contratante</label>
                                        <SelectField
                                            value="1"
                                            className="form-control disabled"
                                            options={['Teste A', 'Teste B']}
                                            valor="uma unidade contratante"
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label>Unidade Realizadora</label>
                                        <SelectField
                                            value="1"
                                            className="form-control disabled"
                                            options={['Teste A', 'Teste B']}
                                            valor="uma unidade realizadora"
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label>Unidade Operacional</label>
                                        <SelectField
                                            value="1"
                                            className="form-control disabled"
                                            options={['Teste A', 'Teste B']}
                                            valor="uma unidade operacional"
                                            onChange={this.onChange} />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <label>Cordenador</label>
                                        <SelectField
                                            value="1"
                                            className="form-control disabled"
                                            options={['Teste A', 'Teste B']}
                                            valor="um coordenador"
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label>Pesquisador</label>
                                        <SelectField
                                            value="1"
                                            className="form-control disabled"
                                            options={['Teste A', 'Teste B']}
                                            valor="um pesquisador"
                                            onChange={this.onChange} />
                                    </div>                                
                                </div>
                            </div>
                        </div>
                    </Form>
                </Section>
                <Section subtitulo="Plano de trabalho">
                    < DataGridPlanoDeTrebalho />
                </Section>
            </div>
        );
    }
});

export default ProjetoDetalheScreen;
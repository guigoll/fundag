import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Section } from '../components/section';
import { Form, InputField, SelectField } from '../components/form';
import { DataGrid } from '../components/grid';

class PlanoTrabalhoScreen extends Component {

    constructor() {
        super();
        this.state = {
            gridVisible: true,
        }
    }

    alterarEstado() {
        var Visible;
        if (this.state.gridVisible) {
            Visible = false
        } else {
            Visible = true
        }
        this.setState({
            gridVisible: Visible
        })
    }

    render() {
        const { gridVisible } = this.state;
        const data = [{
            codigo: 1,
            projeto: 'Projeto A',
            tipo: 'Tipo A',
            parcela: 'Parcela A',
            vencimento: '21/07/1992',
            vigencia: '02/09/2018 à 23/09/2020',
            tipoPeriodDespesas: 'Teste A',
            valor: '9999,9999',
        }, {
            codigo: 2,
            projeto: 'Projeto A',
            tipo: 'Tipo A',
            parcela: 'Parcela A',
            vencimento: '21/07/1992',
            vigencia: '02/09/2018 à 23/09/2020',
            tipoPeriodDespesas: 'Teste A',
            valor: '9999,9999',
        }, {
            codigo: 3,
            projeto: 'Projeto A',
            tipo: 'Tipo A',
            parcela: 'Parcela A',
            vencimento: '21/07/1992',
            vigencia: '02/09/2018 à 23/09/2020',
            tipoPeriodDespesas: 'Teste A',
            valor: '9999,9999',
        }, {
            codigo: 4,
            projeto: 'Projeto A',
            tipo: 'Tipo A',
            parcela: 'Parcela A',
            vencimento: '21/07/1992',
            vigencia: '02/09/2018 à 23/09/2020',
            tipoPeriodDespesas: 'Teste A',
            valor: '9999,9999',
        }]

        const columns = [{
            Header: 'Cód.',
            accessor: 'codigo',
            maxWidth: 100
        }, {
            Header: 'Projeto',
            accessor: 'projeto',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Tipo',
            accessor: 'tipo',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Parcela',
            accessor: 'parcela',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Vencimento',
            accessor: 'vencimento',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Vigência',
            accessor: 'vigencia',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Tipo Periodo Despesas',
            accessor: 'tipoPeriodDespesas',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Valor',
            accessor: 'valor',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }]

        const { user } = this.props;
        return (
            <div>
                {gridVisible ?
                    <Section titulo="Plano de Trabalho"
                        icon="lnr lnr-location"
                        subtitulo="Lista de Planos de Trabalho">
                        <div className="panel-content">
                            <button onClick={this.alterarEstado.bind(this)} className="btn btn-primary">Novo</button>
                        </div>
                        <DataGrid columns={columns} data={data} />
                    </Section>
                    :
                    <Section
                        icon="lnr lnr-location"
                        subtitulo="Cadastro de Planos de Trabalho">
                        <Form onSubmit={this.onSubmit}>
                            <div className="row">
                                <div className="panel-content">
                                    <div className="form-group col-md-6">
                                        <label>Projeto</label>
                                        <InputField
                                            type="text"
                                            name="Projeto"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label>Vigência</label>
                                        <div className="row">
                                            <div className="col-md-6">
                                                <InputField
                                                    type="text"
                                                    name="vigenciaInicial"
                                                    className="form-control"
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                            <div className="col-md-6">
                                                <InputField
                                                    type="text"
                                                    name="vigenciaFinal"
                                                    className="form-control"
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label>Tipo</label>
                                        <SelectField
                                            value="1"
                                            className="form-control"
                                            options={['Avulso', 'Contrato']}
                                            valor="tipo de projeto"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label>Tipo Periodo Despesas</label>
                                        <SelectField
                                            value="1"
                                            className="form-control"
                                            options={['Mensal', 'Semanal', 'Bimestral', 'Anual ']}
                                            valor="um tipo de despesas"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                </div>
                                <div className="panel-content">
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <label className="checkbox-inline">
                                                <InputField
                                                    type="radio"
                                                    name="orcamento"
                                                    value="receita"
                                                />
                                                <span>Orçamento de  Receita?</span>
                                            </label>
                                            <label className="checkbox-inline">
                                                <InputField
                                                    type="radio"
                                                    name="orcamento"
                                                    value="despesas"
                                                />
                                                <span>Orçamento de  Despesa?</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="panel-content">

                                    <div className="form-group col-md-2">
                                        <label>Parcela</label>
                                        <InputField
                                            type="text"
                                            name="Projeto"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group col-md-4">
                                        <label>Vencimento</label>
                                        <InputField
                                            type="text"
                                            name="Projeto"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group col-md-2">
                                        <label>Valor</label>
                                        <InputField
                                            type="text"
                                            name="Projeto"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                </div>

                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="panel-content">
                                        <div className="form-group">
                                            <button type="button" onClick={this.alterarEstado.bind(this)} className="btn btn-primary">Salvar</button>&nbsp;&nbsp;
                                        <button type="button" onClick={this.alterarEstado.bind(this)} className="btn btn-default">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Form>
                    </Section>
                }
            </div >
        );
    }
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    };
}

export default connect(mapStateToProps)(PlanoTrabalhoScreen)

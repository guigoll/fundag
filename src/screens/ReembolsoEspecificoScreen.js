import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Section } from '../components/section';
import { Form, InputField, SelectField, TextareaField } from '../components/form';
import { DataGrid } from '../components/grid';
import { Button } from '../components/button';
import { Modal } from '../components/modal';

import {ReembolsoAnexo} from './'

class ReembolsoEspecificoScreen extends Component {

    constructor() {
        super();
        this.state = {
            descFiltro: "Últimos Lançamentos",
            alterDesc: true
        }
    }
    
    alterarDesc() {
        var desc;
        var alt;
        if (this.state.alterDesc) {
            desc = "Lançamentos do filtro";
            alt = false;
        } else {
            desc = "Últimos Lançamentos";
            alt = true;
        }
        this.setState({
            descFiltro: desc,
            alterDesc: alt
        })
    }
    render() {
        const desc = this.state.descFiltro
        const data = [{
            arquivo: 'README.md',
            tipo: 'PDF',
            comentario: 'Descrição de teste para o arquivo',
        }]

        const columns = [{
            Header: 'Arquivo',
            accessor: 'arquivo',
        }, {
            Header: 'Tipo',
            accessor: 'tipo',
            maxWidth: 100,
            Cell: props => <span className='text-left'>{props.value}</span>

        }, {
            Header: 'Comentário',
            accessor: 'comentario',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: '',
            accessor: 'acao',
            maxWidth: 80,
            Cell: props => [
                <Button className="btn btn-danger" icon="fa fa-trash-o" />
            ]
        }]

        return (
            <div>
                <Section subtitulo="Cadastrar Lançamento">
                    <div className="panel-content">
                        <Form onSubmit={this.onSubmit}>
                            <div className="row">
                                <div className="panel-content">
                                    <div className="form-group ">
                                        <div className="row">
                                            <div className="col-md-3">
                                                <label>Tipo</label>
                                                <SelectField
                                                    value="1"
                                                    className="form-control"
                                                    options={['Custeio', 'Diaria', 'Viagem']}
                                                    valor="um projeto"
                                                    onChange={this.onChange} />
                                            </div>
                                            <div className="col-md-3">
                                                <label>Data</label>
                                                <InputField
                                                    type="text"
                                                    name="dataFim"
                                                    className="form-control"
                                                    placeholder="preencha uma descrição"
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                            <div className="col-md-3">
                                                <label>Atividade</label>
                                                <SelectField
                                                    value="1"
                                                    className="form-control"
                                                    options={['Atividade A', 'Atividade B']}
                                                    valor="um projeto"
                                                    onChange={this.onChange} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div className="panel-content">
                                    <div className="form-group ">
                                        <div className="row">
                                            <div className="col-md-3">
                                                <label>Valor</label>
                                                <InputField
                                                    type="text"
                                                    name="valor"
                                                    className="form-control"
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                            <div className="col-md-3">
                                                <label>Pessoas</label>
                                                <InputField
                                                    type="text"
                                                    name="nPessoas"
                                                    className="form-control"
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                            <div className="col-md-3">
                                                <label>Diarias</label>
                                                <InputField
                                                    type="text"
                                                    name="nDiárias"
                                                    className="form-control"
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="panel-content">
                                    <div className="form-group ">
                                        <label>Locais</label>
                                        <TextareaField
                                            name="nDiárias"
                                            className="form-control col-sm-9"
                                        />
                                    </div>
                                </div>
                            </div>
                        </Form>
                        <div className="row">
                            <div className="panel-content">
                                <div className="form-group">
                                    <button type="button" className="btn btn-danger">Excluir</button>&nbsp;&nbsp;
                                        <button type="button" className="btn btn-default">Cancelar</button>&nbsp;&nbsp;
                                        <button type="button" className="btn btn-primary">Enviar</button>&nbsp;&nbsp;
                                    </div>
                            </div>
                        </div>
                    </div>
                </Section>

                <Section subtitulo="Lista de anexo">
                    <div className="panel-content row">

                        <Modal body={<ReembolsoAnexo />}
                               title="Anexar Arquivos"
                               btnClass="btn btn-default"
                               btnDesc="Adicionar Anexo"
                               btnIcon="fa fa-plus-square"
                              />

                    </div>
                    <DataGrid columns={columns} data={data} />
                </Section>
            </div>
        );
    }
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    };
}

export default connect(mapStateToProps)(ReembolsoEspecificoScreen)

import React, {Component} from 'react';
import {Switch, Route } from 'react-router-dom';

import { Navbar, NavBarMenu, Search } from '../components/header';
import { LeftSideBar, LeftSideBarNav, SideBarScroll } from '../components/sidebar';
import { UserAccount, UserPhoto } from '../components/profile';
import { MainContent } from '../components/main';

import {
  Login,
  Home,
  Projeto,
  PlanoTrabalho,
  ProjetoDetalhe,
  SolicitacoesProjeto,
  RelatorioDespesas,
  CentroDeCusto,
  ContaContabil,
  Adiantamento,
  Reembolso,
  ReembolsoLancamento,
  ReembolsoAnexo,
  ReembolEspecifico,
  ReembolsoGenerico,
  Extrato,
} from './';

const LayoutScreen = () => (
  <div>
  <Navbar>
    <Search />
    <NavBarMenu />
  </Navbar>
  <LeftSideBar>
    <UserAccount>
      <UserPhoto />
    </UserAccount>
    <LeftSideBarNav />
  </LeftSideBar>
  <MainContent>
  <Switch>
      <Route path="/login" component={Login} />
      <Route path="/home" component={Home} />
      <Route path="/projetos" component={Projeto} />
      <Route path="/planodetrabalho" component={PlanoTrabalho} />
      <Route path="/relatoriodespesas" component={RelatorioDespesas} />
      <Route path="/centrodecustos" component={CentroDeCusto} />
      <Route path="/contascontabeis" component={ContaContabil} />
      <Route path="/adiantamento" component={Adiantamento} />
      <Route path="/reembolso" component={Reembolso} />
      <Route path="/reembolsolancamento" component={ReembolsoLancamento} />
      <Route path="/reembolsoanexo" component={ReembolsoAnexo} />
      <Route path="/reembolsoespecifico" component={ReembolEspecifico} />
      <Route path="/reembolsogenerico" component={ReembolsoGenerico} />
      <Route path="/projetodetalhe" component={ProjetoDetalhe} />
      <Route path="/extrato" component={Extrato} />
      <Route path="/solicitacoesprojeto" component={SolicitacoesProjeto} />
  </Switch>
  </MainContent>
  </div>
);

export default LayoutScreen;

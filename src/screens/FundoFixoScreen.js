import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Section } from '../components/section';
import { Form, InputField, SelectField } from '../components/form';
import { DataGrid } from '../components/grid';

const FundoFixoScreen = (props) => ({
    render() {
        const { user } = this.props;
        return (
            <div>
                <Section titulo="Plano de Trabalho"
                    icon="lnr lnr-location"
                    subtitulo="Lista de Planos de Trabalho">
                    <DataGrid />
                </Section>

                <Section titulo="Plano de Trabalho"
                    icon="lnr lnr-location"
                    subtitulo="Cadastro de Planos de Trabalho">
                    <Form onSubmit={this.onSubmit}>
                        <div className="row">
                            <div className="panel-content">
                                <div className="form-group col-md-6">
                                    <label>Projeto</label>
                                    <InputField
                                        type="text"
                                        name="Projeto"
                                        className="form-control"
                                        onChange={this.onChange}
                                    />
                                </div>
                                <div className="form-group col-md-6">
                                    <label>Vigência</label>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <InputField
                                                type="text"
                                                name="vigenciaInicial"
                                                className="form-control"
                                                onChange={this.onChange}
                                            />
                                        </div>
                                        <div className="col-md-6">
                                            <InputField
                                                type="text"
                                                name="vigenciaFinal"
                                                className="form-control"
                                                onChange={this.onChange}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group col-md-6">
                                    <label>Tipo</label>
                                    <SelectField
                                        value="1"
                                        className="form-control"
                                        options={['Avulso', 'Contrato']}
                                        onChange={this.onChange}
                                    />
                                </div>
                                <div className="form-group col-md-6">
                                    <label>Tipo Periodo Despesas</label>
                                    <SelectField
                                        value="1"
                                        className="form-control"
                                        options={['Mensal', 'Semanal', 'Bimestral', 'Anual ']}
                                        onChange={this.onChange}
                                    />
                                </div>
                            </div>
                            <div className="panel-content">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label className="checkbox-inline">
                                            <InputField
                                                type="radio"
                                                name="orcamento"
                                                value="receita"
                                            />
                                            <span>Orçamento de  Receita?</span>
                                        </label>
                                        <label className="checkbox-inline">
                                            <InputField
                                                type="radio"
                                                name="orcamento"
                                                value="despesas"
                                            />
                                            <span>Orçamento de  Despesa?</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="panel-content">

                                <div className="form-group col-md-2">
                                    <label>Parcela</label>
                                    <InputField
                                        type="text"
                                        name="Projeto"
                                        className="form-control"
                                        onChange={this.onChange}
                                    />
                                </div>
                                <div className="form-group col-md-4">
                                    <label>Vencimento</label>
                                    <InputField
                                        type="text"
                                        name="Projeto"
                                        className="form-control"
                                        onChange={this.onChange}
                                    />
                                </div>
                                <div className="form-group col-md-2">
                                    <label>Valor</label>
                                    <InputField
                                        type="text"
                                        name="Projeto"
                                        className="form-control"
                                        onChange={this.onChange}
                                    />
                                </div>
                            </div>

                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <button type="button" className="btn btn-primary">Salvar</button>&nbsp;&nbsp;
                                            <button type="button" className="btn btn-default">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                </Section>
            </div >
        );
    }
});

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    };
}

export default connect(mapStateToProps)(FundoFixoScreen)

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Section } from '../components/section';
import { Form, InputField, SelectField } from '../components/form';
import { DataGrid } from '../components/grid';
import { Button } from '../components/button';

class ReembolsoAnexoScreen extends Component {

    constructor() {
        super();
        this.state = {
            descFiltro: "Últimos Lançamentos",
            alterDesc: true
        }
    }

    alterarDesc() {
        var desc;
        var alt;
        if (this.state.alterDesc) {
            desc = "Lançamentos do filtro";
            alt = false;
        } else {
            desc = "Últimos Lançamentos";
            alt = true;
        }
        this.setState({
            descFiltro: desc,
            alterDesc: alt
        })
    }
    render() {
        const desc = this.state.descFiltro
        const data = [{
            arquivo: 'README.md',
            tipo: 'PDF',
            comentario: 'Descrição de teste para o arquivo',
        }]

        const columns = [{
            Header: 'Arquivo',
            accessor: 'arquivo',
        }, {
            Header: 'Tipo',
            accessor: 'tipo',
            maxWidth: 100,
            Cell: props => <span className='text-left'>{props.value}</span>

        }, {
            Header: 'Comentário',
            accessor: 'comentario',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: '',
            accessor: 'acao',
            maxWidth: 80,
            Cell: props => [
                <Button className="btn btn-danger" icon="fa fa-trash-o" />
            ]
        }]

        return (
            <div>
                <Section subtitulo="Selecione um anexo">
                    <div className="panel-content">
                        <Form onSubmit={this.onSubmit}>
                            <div className="form-group col-md-4">
                                <InputField
                                    type="file"
                                    name="anexo"
                                    className="form-control"
                                    placeholder="selecione um arquivo"
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="form-group col-md-5">
                                <InputField
                                    type="text"
                                    name="dataFim"
                                    className="form-control"
                                    placeholder="preencha uma descrição"
                                    onChange={this.onChange}
                                />
                            </div>
                        </Form>
                        <button onClick={this.alterarDesc.bind(this)} className="btn btn-primary">
                            <i className="fa fa-plus-square"></i>
                            Adicionar
                        </button>
                    </div>
                </Section>

                <Section subtitulo="Lista de Anexo">
                    <div className="row">
                        <div className="panel-content">
                            <DataGrid columns={columns} data={data} />
                        </div>
                    </div>
                </Section>
            </div >
        );
    }
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    };
}

export default connect(mapStateToProps)(ReembolsoAnexoScreen)

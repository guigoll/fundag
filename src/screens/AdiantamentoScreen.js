import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Section } from '../components/section';
import { Form, InputField, SelectField, TextareaField } from '../components/form';
import { DataGridAprova } from '../components/grid';
import { ButtonDropdow } from '../components/button';

class AdiantamentoScreen extends Component {

    constructor() {
        super();
        this.state = {
            gridVisible: true,
        }
    }

    alterarEstado() {
        var Visible;
        if (this.state.gridVisible) {
            Visible = false
        } else {
            Visible = true
        }
        this.setState({
            gridVisible: Visible
        })
    }

    render() {
        const { gridVisible } = this.state;
        const data = [{
            codigo: 'AD-0001',
            projeto: 'projeto A',
            solicitante: 'Guilherme',
            atividade: 'baixa',
            data: '05/09/2018',
            banco: 'Bradesco'
        }, {
            codigo: 'AD-0002',
            projeto: 'projeto A',
            solicitante: 'Guilherme',
            atividade: 'baixa',
            data: '05/09/2018',
            banco: 'Caixa Econômica'
        }, {
            codigo: 'AD-0003',
            projeto: 'projeto A',
            solicitante: 'Guilherme',
            atividade: 'baixa',
            data: '05/09/2018',
            banco: 'Santander'
        }, {
            codigo: 'AD-0004',
            projeto: 'projeto B',
            solicitante: 'Leonardo',
            atividade: 'baixa',
            data: '05/09/2018',
            banco: 'Itaú'
        }, {
            codigo: 'AD-0005',
            projeto: 'projeto C',
            solicitante: 'Marcelo',
            atividade: 'alta',
            data: '05/09/2018',
            banco: 'Banco do Brasil'
        }, {
            codigo: 'AD-0006',
            projeto: 'projeto A',
            solicitante: 'Guilherme',
            atividade: 'baixa',
            data: '05/09/2018',
            banco: 'Bradesco'
        },]

        const columns = [{
            Header: '',
            accessor: 'edicao',
            Cell: props => [
                <ButtonDropdow options={[
                    { description: 'continuar' },
                    { description: 'visualizar' },
                    { description: 'descartar' },
                    { description: 'Enviar para aprovação' }
                ]} />
            ]
        }, {
            Header: 'Código',
            accessor: 'codigo',
            headerClassName: 'text-left',
            Cell: props => <span>{props.value}</span>
        }, {
            Header: 'Solicitante',
            headerClassName: 'text-left',
            accessor: 'solicitante',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Projeto',
            headerClassName: 'text-left',
            accessor: 'projeto',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Atividade',
            accessor: 'atividade',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Data',
            headerClassName: 'text-left',
            accessor: 'data',
            Cell: props => <span className='text-right'>{props.value}</span>
        }, {
            Header: 'Banco',
            headerClassName: 'text-left',
            accessor: 'banco',
            Cell: props => <span className='text-right'>{props.value}</span>
        }]
        return (
            <div>
                {gridVisible ?
                    <Section titulo="Adiantamento" subtitulo="Lista de Adiantemento">
                        <div className="panel-content">
                            <button onClick={this.alterarEstado.bind(this)} className="btn btn-primary">Novo</button>
                        </div>
                        <DataGridAprova columns={columns} data={data} />
                    </Section>
                    :
                    <Section
                        titulo="Adiantamento"
                        subtitulo="Cadastro de Adiantamento">
                        <Form onSubmit={this.onSubmit}>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="panel-content">

                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label>Solicitante</label>
                                                    <SelectField
                                                        value="1"
                                                        className="form-control "
                                                        options={['Solicitante A', 'Solicitante B']}
                                                        banco="um solicitante"
                                                        onChange={this.onChange} />
                                                </div>
                                                <div className="col-md-6">
                                                    <label>Projeto</label>
                                                    <SelectField
                                                        value="1"
                                                        className="form-control "
                                                        options={['Projeto A', 'Projeto B']}
                                                        banco="um projeto"
                                                        onChange={this.onChange} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-md-8">
                                                    <label>Atividade</label>
                                                    <SelectField
                                                        value="1"
                                                        className="form-control "
                                                        options={['Combustivel', 'Diárias', 'Custeio']}
                                                        banco="uma atividade"
                                                        onChange={this.onChange} />
                                                </div>
                                                <div className="col-md-4">
                                                    <label>Data Previsão</label>
                                                    <InputField
                                                        type="text"
                                                        name="titulo"
                                                        className="form-control "
                                                        onChange={this.onChange}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label>Finalidade</label>
                                            <TextareaField
                                                type="text"
                                                name="finalizadade"
                                                rows="11"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="panel-content">

                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-md-8">
                                                    <label>Banco</label>
                                                    <InputField
                                                        type="text"
                                                        name="banco"
                                                        className="form-control "
                                                        onChange={this.onChange}
                                                    />
                                                </div>
                                                <div className="col-md-4">
                                                    <label>Agência</label>
                                                    <InputField
                                                        type="text"
                                                        name="agencia"
                                                        className="form-control "
                                                        onChange={this.onChange}
                                                    />
                                                </div>
                                            </div>
                                        </div>


                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label>Conta</label>
                                                    <InputField
                                                        type="text"
                                                        name="conta"
                                                        className="form-control "
                                                        onChange={this.onChange}
                                                    />
                                                </div>
                                                <div className="col-md-2">
                                                    <label>Digíto</label>
                                                    <InputField
                                                        type="text"
                                                        name="digito"
                                                        className="form-control "
                                                        onChange={this.onChange}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <label>Favorecido</label>
                                            <InputField
                                                type="text"
                                                name="favorecido"
                                                className="form-control "
                                                onChange={this.onChange}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label>CPF</label>
                                                    <InputField
                                                        type="text"
                                                        name="cpf"
                                                        className="form-control "
                                                        onChange={this.onChange}
                                                    />
                                                </div>
                                                <div className="col-md-6">
                                                    <label>RG</label>
                                                    <InputField
                                                        type="text"
                                                        name="rg"
                                                        className="form-control "
                                                        onChange={this.onChange}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label>Observação</label>
                                            <TextareaField
                                                type="text"
                                                name="obsBanco"
                                                row="10"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="panel-content">
                                        <div className="form-group">
                                            <button type="button" onClick={this.alterarEstado.bind(this)} className="btn btn-primary">Salvar</button>&nbsp;&nbsp;
                                        <button type="button" onClick={this.alterarEstado.bind(this)} className="btn btn-default">Cancelar</button>&nbsp;&nbsp;
                                        <button type="button" onClick={this.alterarEstado.bind(this)} className="btn btn-success">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Form>
                    </Section>
                }

            </div >
        );
    }
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    };
}

export default connect(mapStateToProps)(AdiantamentoScreen)

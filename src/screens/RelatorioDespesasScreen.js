import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Section } from '../components/section';
import { Form, InputField, SelectField } from '../components/form';
import { DataGridAprova, DataGridMovimentacao } from '../components/grid';
import { Relatorio, RelatorioBody } from '../components/relatoriodespesas';
import { ButtonDropdow } from '../components/button';

class RelatorioDespesasScreen extends Component {
  render() {
    const data = [{
      tipo: "Adiantamento",
      codigo: 'AD-0001',
      projeto: 'projeto A',
      nome: 'Guilherme',
      criticidade: 'baixa',
      data: '05/09/2018',
      valor: 'R$ 50,00'
    }, {
      tipo: "Adiantamento",
      codigo: 'AD-0002',
      projeto: 'projeto A',
      nome: 'Guilherme',
      criticidade: 'baixa',
      data: '05/09/2018',
      valor: 'R$ 50,00'
    }, {
      tipo: "Prestação de Conta",
      codigo: 'AD-0003',
      projeto: 'projeto A',
      nome: 'Guilherme',
      criticidade: 'baixa',
      data: '05/09/2018',
      valor: 'R$ 50,00'
    }, {
      tipo: "Bolsa",
      codigo: 'AD-0004',
      projeto: 'projeto B',
      nome: 'Leonardo',
      criticidade: 'baixa',
      data: '05/09/2018',
      valor: 'R$ 50,00'
    }, {
      tipo: "Prestação de Conta",
      codigo: 'AD-0005',
      projeto: 'projeto C',
      nome: 'Marcelo',
      criticidade: 'alta',
      data: '05/09/2018',
      valor: 'R$ 60,00'
    }, {
      tipo: "Adiantamento",
      codigo: 'AD-0006',
      projeto: 'projeto A',
      nome: 'Guilherme',
      criticidade: 'baixa',
      data: '05/09/2018',
      valor: 'R$ 50,00'
    },]

    const columns = [{
      Header: 'Tipo',
      headerClassName: 'text-left',
      accessor: 'tipo'
    }, {
      Header: 'Código',
      accessor: 'codigo',
      headerClassName: 'text-left',
      Cell: props => <span>{props.value}</span>
    }, {
      Header: 'Projeto',
      headerClassName: 'text-left',
      accessor: 'projeto',
      Cell: props => <span className='text-left'>{props.value}</span>
    }, {
      Header: 'Nome',
      headerClassName: 'text-left',
      accessor: 'nome',
      Cell: props => <span className='text-left'>{props.value}</span>
    }, {
      Header: 'Criticidade',
      accessor: 'criticidade',
      Cell: props => <span className='text-left'>{props.value}</span>
    }, {
      Header: 'Data',
      headerClassName: 'text-left',
      accessor: 'data',
      Cell: props => <span className='text-right'>{props.value}</span>
    }, {
      Header: 'Valor',
      headerClassName: 'text-left',
      accessor: 'valor',
      Cell: props => <span className='text-right'>{props.value}</span>
    }, {
      Header: '',
      accessor: 'edicao',
      Cell: props => [
        <ButtonDropdow options={[
          { description: 'editar' },
          { description: 'visualizar' },
          { description: 'reprovar' }
        ]} />
      ]
    }]

    const { user } = this.props;
    return (
      <div>
        <Section titulo=" Relatório Despesas"
          icon="lnr lnr-chart-bars"
          subtitulo="Prestação de contas">
           {/* Renderizar o cabeçalho junto com a opção de adicionar as depesas */}
          <Relatorio/>
        </Section>

        <Section subtitulo="Relatórios não enviados">
          <DataGridAprova columns={columns} data={data} />
        </Section>

        <Section subtitulo="Relatórios reprovados(que poderam ser reutilizados)">
          <DataGridAprova columns={columns} data={data} />
        </Section>

      </div>
    );
  }
};

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}

export default connect(mapStateToProps)(RelatorioDespesasScreen)

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { Section } from '../components/section';
import { Form, InputField, SelectField } from '../components/form';

class ReembolsoEditarScreen extends Component {
    render() {
        return (
            <Section
                subtitulo="Editar Reembolso">
                <Form onSubmit={this.onSubmit}>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="panel-content">
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-sm-2">
                                            <label>Código</label>
                                            <InputField
                                                type="text"
                                                name="codigo"
                                                className="form-control"
                                                onChange={this.onChange}
                                            />
                                        </div>
                                        <div className="col-sm-8">
                                            <label>Projeto</label>
                                            <SelectField
                                                value="1"
                                                className="form-control"
                                                options={['Projeto A', 'Projeto B']}
                                                valor="um projeto"
                                                onChange={this.onChange} />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label>Titulo</label>
                                            <InputField
                                                type="text"
                                                name="titulo"
                                                className="form-control"
                                                onChange={this.onChange}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <Section subtitulo="Dados Fornecedor">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <label>Solicitante</label>
                                        <SelectField
                                            value="1"
                                            className="form-control"
                                            options={['Solicitante A', 'Solicitante B']}
                                            valor="um solicitante"
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label>CPF</label>
                                        <InputField
                                            type="text"
                                            name="solicitanteCPF"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Nome</label>
                                        <InputField
                                            type="text"
                                            name="solicitanteNome"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>

                                </div>
                            </Section>
                        </div>
                        <div className="col-md-6">
                            <Section subtitulo="Dados Bancarios">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label>Banco</label>
                                                <SelectField
                                                    value="1"
                                                    className="form-control"
                                                    options={['Banco A', 'Banco B']}
                                                    valor="um banco"
                                                    onChange={this.onChange} />
                                            </div>
                                            <div className="col-md-8">
                                                <label>Agência</label>
                                                <SelectField
                                                    value="1"
                                                    className="form-control"
                                                    options={['Teste A', 'Teste B']}
                                                    valor="um coordenador"
                                                    onChange={this.onChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-sm-9">
                                                <label>C.C.</label>
                                                <InputField
                                                    type="text"
                                                    name="contaCorrente"
                                                    className="form-control"
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                            <div className="col-md-3">
                                                <label>Digíto C.C.</label>
                                                <InputField
                                                    type="text"
                                                    name="digitoConta"
                                                    className="form-control"
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Section>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="panel-content">
                                <div className="form-group">
                                    <button type="button" className="btn btn-primary">Salvar</button>&nbsp;&nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                </Form>
            </Section>
        );
    }
};

export default ReembolsoEditarScreen

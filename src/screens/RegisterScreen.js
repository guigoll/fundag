import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import {Login, Avatar,  Form} from '../components/login';
import {Field, InputField} from '../components/form';
import {Button, ButtonIcon} from '../components/button';

import {userActions} from '../store/actions';
import {validateName} from '../store/helpers';

class Register extends Component{
  constructor(props) {
    super(props);

    this.state = {
      user:{
        name: '',
        email: '',
        password: '',
        sex: '',
        birthday: ''
      },
      submitted: false
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  //Responsavel por alterar o estado do atributo como se fosse fazer um bindcreator
  onChange(e) {
    const { name, value } = event.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [e.target.name]: e.target.value
      }
    });
  }

  onSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    const { user } = this.state;
    const { dispatch } = this.props;
    if (user.name &&  user.email && user.password &&  user.birthday) {
         dispatch(userActions.register(user));
      }

    }
    render(){
      const { registering  } = this.props;
      const { user, submitted } = this.state;
      return(
        <Login>
          <Avatar/>
          <Form onSubmit={this.onSubmit}>
            <InputField
              type="text"
              name="name"
              validate={validateName}
              value={user.name}
              placeholder="Nome"
              isValid={this.state.user.name.isValid}
              onChange={this.onChange}
              />
            {submitted && !name &&
                <div className="help-block">Preencha o Nome</div>
              }
            <InputField
              type="text"
              name="email"
              validate={validateName}
              value={user.email}
              placeholder="Email"
              onChange={this.onChange}
              />
              {submitted && !name &&
                  <div className="help-block">Preencha o Email</div>
                }
            <InputField
              type="text"
              name="password"
              value={user.password}
              validate={validateName}
              placeholder="Senha"
              onChange={this.onChange}
              />
              {submitted && !name &&
                  <div className="help-block">Preencha a Senha</div>
                }
            <div className="form-check form-check-inline">
              <InputField
                type="radio"
                className= "form-check-input"
                value= "Feminino"
                name="sex"
                />
              <label className="form-check-label">
                Feminino
              </label>
            </div>
            <div className="form-check form-check-inline">
              <InputField
                type="radio"
                className= "form-check-input"
                value= "Masculino"
                name="sex"
                />
              <label className="form-check-label">
                Masculino
              </label>
            </div>
            <InputField
              type="text"
              name="birthday"
              validate={validateName}
              value={user.birthday}
              placeholder="Data de nascimento"
              onChange={this.onChange}
              />
              {submitted && !name &&
                  <div className="help-block">Preencha a Data de aniversário</div>
                }
            <Button
              type="submit"
              className="btn btn-info btn-block login"
              Text="Cadastrar"
              />
          </Form>
          <div className="text-form-footer">
            <Link to="/login" className="text-center criar">  Já usa o Permata? Entrar</Link>
          </div>
        </Login>
      );
    }
  }

  function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
      registering
    };
  }

  export  default connect(mapStateToProps)(Register);

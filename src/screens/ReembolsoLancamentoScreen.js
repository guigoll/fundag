import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { Section } from '../components/section';
import { DataGridAprova  } from '../components/grid';
import { ButtonDropdow } from '../components/button';
import { Modal } from '../components/modal';

import {ReembolsoEditar, ReembolsoAnexo,  ReembolEspecifico, ReembolsoGenerico } from './'

class ReembolsoLancamentoScreen extends Component {
    constructor() {
        super();
        this.state = {
            addItem: false,
        }
    }
    alterarEstado() {
        var Visible;
        if (this.state.gridVisible) {
            Visible = false
        } else {
            Visible = true
        }
        this.setState({
            gridVisible: Visible
        })
    }
    render() {
        const { gridVisible } = this.state;
        const data = [{
            data: "01/11/2018",
            tipo: "almoço",
            comentario: "almoço c/ cliente",
            descricao: "2 pessoas",
            politica: "",
            valor: "15,00"
        },{
            data: "01/11/2018",
            tipo: "diversos",
            comentario: "apostila curso",
            descricao: "-",
            politica: "",
            valor: "250,00"
        },{
            data: "01/11/2018",
            tipo: "diversos",
            comentario: "apostila curso",
            descricao: "-",
            politica: "",
            valor: "250,00"
        },{
            data: "01/11/2018",
            tipo: "diversos",
            comentario: "apostila curso",
            descricao: "-",
            politica: "",
            valor: "250,00"
        },{
            data: "01/11/2018",
            tipo: "diversos",
            comentario: "apostila curso",
            descricao: "-",
            politica: "",
            valor: "250,00"
        },{
            data: "01/11/2018",
            tipo: "diversos",
            comentario: "apostila curso",
            descricao: "-",
            politica: "",
            valor: "250,00"
        }];
        const columns = [{
            Header: 'Data',
            headerClassName: 'text-left',
            accessor: 'data',
            Cell: props => <span className='text-right'>{props.value}</span>
          }, {
              Header: 'Tipo',
              headerClassName: 'text-left',
              accessor: 'tipo',
              Cell: props => <span className='text-right'>{props.value}</span>
            },{
                Header: 'Comentario',
                headerClassName: 'text-left',
                accessor: 'comentario',
                Cell: props => <span className='text-right'>{props.value}</span>
              },{
                  Header: 'Descrição',
                  headerClassName: 'text-left',
                  accessor: 'descricao',
                  Cell: props => <span className='text-right'>{props.value}</span>
                },{
                    Header: 'Politica',
                    headerClassName: 'text-left',
                    accessor: 'politica',
                    Cell: props => <span className='text-right'>{props.value}</span>
                  },{
                      Header: 'Valor',
                      headerClassName: 'text-left',
                      accessor: 'valor',
                      Cell: props => <span className='text-right'>{props.value}</span>
                    },{
            Header: '',
            accessor: 'edicao',
            Cell: props => [
              <ButtonDropdow options={[
                { icon: "lnr lnr-pencil", description: 'editar', Component: <ReembolsoEditar /> },
                { icon: "lnr lnr-sun", description: 'excluir' },
                { icon: "lnr lnr-trash", description: 'rateio'},
                { icon: "lnr lnr-trash", description: 'anexo',  Component: <ReembolsoAnexo /> }
              ]} />
            ]
          }];
        return (
            <div>
                <Section
                    titulo="Reembolso"
                    subtitulo="Dados da solicitação">
                    <div className="panel-content row">

                          <Modal body={<ReembolsoAnexo />}
                                 title="Anexar Arquivos"
                                 btnClass="btn btn-default"
                                 btnDesc="Anexo"
                                 btnIcon="fa fa-plus-square"
                                />

                          <Modal body={<ReembolsoEditar />}
                                 title="Editar Reembolso"
                                 btnClass="btn btn-warning"
                                 btnDesc="Editar"
                                />

                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" href="#combinedOne" className="collapse in">
                                    <table className="table table-lancamento">
                                        <thead className="table-item">
                                            <tr>
                                                <td><strong>Destaque</strong></td>
                                                <td><strong>Titulo</strong></td>
                                                <td><strong>Código</strong></td>
                                                <td><strong>Projeto</strong></td>
                                            </tr>
                                        </thead>
                                    </table>
                                </a>
                            </h4>
                        </div>
                        <div id="combinedOne" className="panel-collapse collapse in">
                            <table className="table">
                                <tbody className="table-item">
                                    <tr>
                                        <td>Projeto</td>
                                        <td>Projeto modelo teste</td>
                                        <td>AB 9999999</td>
                                        <td>Projeto A</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </Section>

                <Section subtitulo="Resumo válido">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" href="#combinedTwo" className="collapse in">
                                    <table className="table table-lancamento">
                                        <thead className="table-item">
                                            <tr>
                                                <td><strong>Destaque</strong> </td>
                                                <td><strong>Valor Total</strong></td>
                                                <td><strong>Valor Adiantado</strong></td>
                                                <td><strong>Saldo</strong></td>
                                            </tr>
                                        </thead>
                                    </table>
                                </a>
                            </h4>
                        </div>
                        <div id="combinedTwo" className="panel-collapse collapse in">
                            <table className="table">
                                <tbody className="table-item">
                                    <tr>
                                        <td>1011</td>
                                        <td>800,00</td>
                                        <td>300,00</td>
                                        <td>500</td>
                                    </tr>
                                    <tr>
                                        <td>1011</td>
                                        <td>500,00</td>
                                        <td>300,00</td>
                                        <td>200</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </Section>

                <Section subtitulo="Lançamentos">
                    <div className="panel-content row">

                      <Modal body={<ReembolEspecifico />}
                             title="Lançar Reembolso"
                             btnClass="btn btn-warning"
                             btnDesc="Novo Lançamento"
                            />

                    </div>

                    <div className="col-md-12 row">
                        <a data-toggle="collapse" href="#combinedTree" className="collapse in pull-right">x</a>
                    </div>

                    <div id="combinedTree" className="panel-collapse collapse in">
                        <DataGridAprova  columns={columns} data={data} />
                    </div>

                </Section>

                <div className="row">
                    <div className="col-md-6">
                        <div className="panel-content">
                            <div className="form-group">
                                <button type="button" onClick={this.alterarEstado.bind(this)} className="btn btn-primary">Enviar</button>&nbsp;&nbsp;
                                        <button type="button" onClick={this.alterarEstado.bind(this)} className="btn btn-default">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default ReembolsoLancamentoScreen;

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Section } from '../components/section';
import { Form, InputField, SelectField, MultiSelectField } from '../components/form';
import { DataGrid } from '../components/grid';

class SolicitacoesProjetoScreen extends Component {


    render() {
        const data = [{
            codigo: 1,
            projeto: 'Projeto A',
            unidContratante: 'Teste A',
            unidRealizadora: 'Teste A',
            unidOperacional: '21/07/1992',
            titulo: 'Projeto teste A',
            objetivos: 'Teste A',
            tipoInstituicao: 'Privada',
            coordenador: 'Cordenador 1',
            pesquisador: 'Pesquisador 1',
            codprojeto: 1
        }, {
            codigo: 2,
            projeto: 'Projeto B',
            unidContratante: 'Teste B',
            unidRealizadora: 'Teste B',
            unidOperacional: '21/07/1992',
            titulo: 'Projeto teste A',
            objetivos: 'Teste B',
            tipoInstituicao: 'Privada',
            coordenador: 'Cordenador 1',
            pesquisador: 'Pesquisador 1',
            codprojeto: 1
        }, {
            codigo: 3,
            projeto: 'Projeto C',
            unidContratante: 'Teste C',
            unidRealizadora: 'Teste C',
            unidOperacional: '21/07/1992',
            titulo: 'Projeto teste C',
            objetivos: 'Teste C',
            tipoInstituicao: 'Privada',
            coordenador: 'Cordenador 3',
            pesquisador: 'Pesquisador 3',
            codprojeto: 1
        }, {
            codigo: 4,
            projeto: 'Projeto D',
            unidContratante: 'Teste D',
            unidRealizadora: 'Teste D',
            unidOperacional: '21/07/1992',
            titulo: 'Projeto teste D',
            objetivos: 'Teste D',
            tipoInstituicao: 'Publica',
            coordenador: 'Cordenador 1',
            pesquisador: 'Pesquisador 4',
            codprojeto: 3
        }]

        const columns = [{
            Header: 'Cód.',
            accessor: 'codigo',
            maxWidth: 100
        }, {
            Header: 'Projeto',
            accessor: 'projeto',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Unidade Contratante',
            accessor: 'unidContratante',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Unidade Realizadora',
            accessor: 'unidRealizadora',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Unidade Operacional',
            accessor: 'unidOperacional',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Programa',
            accessor: 'programa',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Titulo',
            accessor: 'titulo',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Obejetivos',
            accessor: 'objetivos',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Tipo Instiuição',
            accessor: 'tipoInstituicao',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Cordenador',
            accessor: 'coordenador',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Pesquisador',
            accessor: 'pesquisador',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }, {
            Header: 'Cód. Projeto',
            accessor: 'codprojeto',
            Cell: props => <span className='text-left'>{props.value}</span> // Custom cell components!
        }]
        const { user } = this.props;
        return (
            <div>

                <Section titulo="Solicitações"
                    icon="fa fa-envelope"
                    subtitulo="Filtrar Solicitações">
                    <Form onSubmit={this.onSubmit}>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-3">
                                                <label>Data Inicio</label>
                                                <InputField
                                                    type="text"
                                                    name="codigo"
                                                    className="form-control "
                                                    onChange={this.onChange}
                                                />
                                            </div> <div className="col-md-3">
                                                <label>Data Fim</label>
                                                <InputField
                                                    type="text"
                                                    name="titulo"
                                                    className="form-control "
                                                    onChange={this.onChange}
                                                />
                                            </div>
                                            <div className="col-md-6">
                                                <label>Solicitante</label>
                                                <SelectField
                                                    value="1"
                                                    className="form-control "
                                                    options={['Teste A', 'Teste B']}
                                                    valor="um solicitante"
                                                    onChange={this.onChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>Projeto</label>
                                        <SelectField
                                            value="1"
                                            className="form-control "
                                            options={['Projeto A', 'Projeto B']}
                                            valor="um projeto"
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="row">
                                        <div className="form-group col-md-6">
                                            <label>Unidade Contratante</label>
                                            <SelectField
                                                value="1"
                                                className="form-control "
                                                options={['Teste A', 'Teste B']}
                                                valor="uma unidade contratante"
                                                onChange={this.onChange} />
                                        </div>
                                        <div className="form-group  col-md-6">
                                            <label>Unidade Realizadora</label>
                                            <SelectField
                                                value="1"
                                                className="form-control "
                                                options={['Teste A', 'Teste B']}
                                                valor="uma unidade realizadora"
                                                onChange={this.onChange} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="panel-content">
                                    <div className="form-group">
                                        <label>Cordenador</label>
                                        <SelectField
                                            value="1"
                                            className="form-control "
                                            options={['Teste A', 'Teste B']}
                                            valor="um coordenador"
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label>Pesquisador</label>
                                        <MultiSelectField                                                                                 
                                            options={['Pesquisador A', 'Pesquisador B', 'Pesquisador C', 'Pesquisador D']}
                                            valor="um pesquisador"
                                            onChange={this.onChange} />
                                    </div>

                                    <div className="row">
                                        <div className="form-group col-md-6">
                                            <label>Tipo</label>
                                            <SelectField
                                                value="1"
                                                className="form-control "
                                                options={['Teste A', 'Teste B']}
                                                valor="um pesquisador"
                                                onChange={this.onChange} />
                                        </div>
                                        <div className="form-group col-md-6">
                                            <label>Situação</label>
                                            <SelectField
                                                value="1"
                                                className="form-control "
                                                options={['Teste A', 'Teste B']}
                                                valor="um pesquisador"
                                                onChange={this.onChange} />
                                        </div>
                                        <div className="col-md-9"></div>
                                        <div className="col-md-3">
                                            <butto className="btn btn-primary btn-block">Filtrar</butto>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </Form>
                </Section>

                <Section
                    subtitulo="Lista de Solicitações">
                    <DataGrid columns={columns} data={data} />
                </Section>
            </div >
        );
    }
};

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    };
}

export default connect(mapStateToProps)(SolicitacoesProjetoScreen)

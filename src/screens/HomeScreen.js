import React, { Component } from 'react';
import { connect } from 'react-redux';

import { SectionGenerico } from '../components/section';
import { DataGridAprova } from '../components/grid';
import { Table } from '../components/table';
import { LineChart, SaldoChart } from '../components/chart';
import { ButtonDropdow } from '../components/button';

import {ReembolsoEditar} from './'

const HomeScreen = props => ({
  render() {
    const data = [{
      tipo: "Adiantamento",
      codigo: 'AD-0001',
      projeto: 'projeto A',
      nome: 'Guilherme',
      criticidade: 'baixa',
      data: '05/09/2018',
      valor: 'R$ 50,00',
      acao: <ButtonDropdow options={[
        { description: 'editar', modal: true , Component: <ReembolsoEditar/> },
        { description: 'visualizar' },
        { description: 'reprovar' }
       ]} />
    }, {
      tipo: "Adiantamento",
      codigo: 'AD-0002',
      projeto: 'projeto A',
      nome: 'Guilherme',
      criticidade: 'baixa',
      data: '05/09/2018',
      valor: 'R$ 50,00',
      acao: <ButtonDropdow options={[
        { description: 'editar', modal: true , Component: <ReembolsoEditar/> },
        { description: 'visualizar' },
        { description: 'reprovar' }
       ]} />
    }, {
      tipo: "Prestação de Conta",
      codigo: 'AD-0003',
      projeto: 'projeto A',
      nome: 'Guilherme',
      criticidade: 'baixa',
      data: '05/09/2018',
      valor: 'R$ 50,00',
      acao: <ButtonDropdow options={[
        { description: 'editar', modal: true , Component: <ReembolsoEditar/> },
        { description: 'visualizar' },
        { description: 'reprovar' }
       ]} />
    }, {
      tipo: "Bolsa",
      codigo: 'AD-0004',
      projeto: 'projeto B',
      nome: 'Leonardo',
      criticidade: 'baixa',
      data: '05/09/2018',
      valor: 'R$ 50,00',
      acao: <ButtonDropdow options={[
        { description: 'editar', modal: true , Component: <ReembolsoEditar/> },
        { description: 'visualizar' },
        { description: 'reprovar' }
       ]} />
    }, {
      tipo: "Prestação de Conta",
      codigo: 'AD-0005',
      projeto: 'projeto C',
      nome: 'Marcelo',
      criticidade: 'alta',
      data: '05/09/2018',
      valor: 'R$ 60,00',
      acao: <ButtonDropdow options={[
        { description: 'editar', modal: true , Component: <ReembolsoEditar/> },
        { description: 'visualizar' },
        { description: 'reprovar' }
       ]} />
    }, {
      tipo: "Adiantamento",
      codigo: 'AD-0006',
      projeto: 'projeto A',
      nome: 'Guilherme',
      criticidade: 'baixa',
      data: '05/09/2018',
      valor: 'R$ 50,00',
      acao: <ButtonDropdow options={[
        { description: 'editar', modal: true , Component: <ReembolsoEditar/> },
        { description: 'visualizar' },
        { description: 'reprovar' }
       ]} />
    }]

    const columns = [
      ['Tipo', 'tipo'],
      ['Codigo', 'codigo'],
      ['Projeto', 'projeto'],
      ['Nome', 'nome'],
      ['Criticidade', 'criticidade'],
      ['Data', 'data'],
      ['Valor', 'valor'],
      ['Ação', 'acao']
    ];


    const { user } = this.props;
    return (
      <div>
        <SectionGenerico titulo="Dashboard FUNDAG"
          icon="fa fa-pie-chart"
          options={[{ col: "col-md-12", subtitulo: "Solicitações em aprovação", component: <Table columns={columns} data={data} /> }
          ]}>
        </SectionGenerico>

        <SectionGenerico
          options={[{ col: "col-md-12", subtitulo: "Solicitação em edição", component: <Table columns={columns} data={data} /> }
          ]}>
        </SectionGenerico>

        <SectionGenerico
          options={[{ col: "col-md-12", subtitulo: "Últimas Solicitações", component: <Table columns={columns} data={data} /> }
          ]}>
        </SectionGenerico>

        <SectionGenerico
          options={[
            {
              col: "col-md-6",
              subtitulo: 'Projeto x Saldo',
              component: <LineChart />
            },
            {
              col: "col-md-6",
              subtitulo: 'Detalhes do Projeto',
              component: <SaldoChart />
            }]}>
        </SectionGenerico>
      </div>
    );
  }
});

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}

export default connect(mapStateToProps)(HomeScreen)

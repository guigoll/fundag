import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class LoginScreen extends Component {
  render() {
    return (

      <div className="vertical-align-wrap">
        <div className="vertical-align-middle">
          <div className="auth-box">
            <div className="content">
              <div className="header">
                <div className="logo text-center">
                  <img src="images/logo.png" alt="DiffDash" />
                  <p className="lead"></p>
                </div>
                <form className="form-auth-small" action="index.php">

                  <div className="form-group">
                    <label for="signin-email" className="control-label sr-only">E-mail</label>
                    <input type="email" className="form-control" id="signin-email" value="samuel.gold@domain.com" placeholder="preencha o e-mail" />
                  </div>

                  <div className="form-group">
                    <label for="signin-password" className="control-label sr-only">Senha</label>
                    <input type="password" className="form-control" id="signin-password" value="thisisthepassword" placeholder="preencha a senha" />
                  </div>
                  <div className="form-group clearfix">
                    <label className="fancy-checkbox element-left">
                      <input type="checkbox" />
                      <span>Salvar?</span>
                    </label>               
                  </div>

                  <Link to="/home" className="btn btn-primary btn-lg btn-block">Acessar</Link>                 

                  <div className="bottom">
                    <span className="helper-text">
                      <i className="fa fa-lock"></i>
                      <a href="page-forgot-password.html">Esqueceu a senha?</a>
                    </span>
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}


export default LoginScreen;

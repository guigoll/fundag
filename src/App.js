import React, { Component } from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from './store/helpers';
import { alertActions } from './store/actions';

import {
  Login,
  Home,
  Projeto,
  PlanoTrabalho,
  RelatorioDespesas,
  CentroDeCusto,
  ContaContabil,
  Adiantamento,
  Reembolso,
  ReembolsoLancamento,
  ReembolsoAnexo,
  ReembolEspecifico,
  ReembolsoGenerico,
  Layout,
  ProjetoDetalhe,
  SolicitacoesProjeto,
  Extrato,
} from './screens';

import { PrivateRoute } from './components/routes';

const AppRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
      <Layout>
        <Component {...props} />
      </Layout>
    )} />
  );

class App extends Component {
  render() {
    return (
          <Router history={history}>
            <Switch>
              <Route  path="/login"  component={Login} />
              <AppRoute path="/home" component={Home} />
              <AppRoute path="/projetos" component={Projeto} />
              <AppRoute path="/planodetrabalho" component={PlanoTrabalho} />
              <AppRoute path="/relatoriodespesas" component={RelatorioDespesas} />
              <AppRoute path="/centrodecustos" component={CentroDeCusto} />
              <AppRoute path="/contascontabeis" component={ContaContabil} />
              <AppRoute path="/adiantamento" component={Adiantamento} />
              <AppRoute path="/reembolso" component={Reembolso} />
              <AppRoute path="/reembolsolancamento" component={ReembolsoLancamento} />
              <AppRoute path="/reembolsoanexo" component={ReembolsoAnexo} />
              <AppRoute path="/reembolsoespecifico" component={ReembolEspecifico} />
              <AppRoute path="/reembolsogenerico" component={ReembolsoGenerico} />
              <AppRoute path="/projetodetalhe" component={ProjetoDetalhe} />
              <AppRoute path="/extrato" component={Extrato} />
              <AppRoute path="/solicitacoesprojeto" component={SolicitacoesProjeto} />
            </Switch>
          </Router>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert
  };
}

export default connect(mapStateToProps)(App);

import axios from 'axios';

import { AuthHeader } from '../helpers';

export const userService = {
  login,
  logout,
  register,
}

const url = "https://staging-permata-api.herokuapp.com";

function login(email, password){
  const requestOptions = {
    method: 'GET',
    headers: {'Content-Type': 'application/json'}
  };
  return axios.post(url + '/auth/sign_in',{email: email, password: password }, requestOptions)
  .then(response => {
    const user = response.data;
    if(user.data.auth_token){
      //Armazena o token
      localStorage.setItem('user', JSON.stringify(user.data));
    }
    return user;
  });
}

function logout() {
  localStorage.removeItem('user');
}


function register(user){
  const requestOptions = {
    method: 'GET',
    headers: {'Content-Type': 'application/json'}
  };
  return axios.post(url + '/auth',{ name: user.name, email: user.email, password: user.password, birthday: user.birthday, sex: user.sex }, requestOptions)
  .then(response => {
    const user = response.data;
    return user;
  });
}

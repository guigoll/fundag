import { combineReducers } from 'redux';

import { authentication } from './AuthenticationReducer';
import { users, registration } from './UserReducer';

export const rootReducer =  combineReducers({
   authentication,
   users,
   registration,
});

import React, { Component } from 'react'


const TextareaField = ({
  value,
  name,
  className,
  rows,
  cols,
  placeholder,
  id,
  onChange,
  onClick,
}) => (  
      <textarea
        id={id}
        className={className}
        value={value}
        name={name}
        rows={rows}
        cols ={cols}
        placeholder={placeholder}
        onChange={onChange}
        onClick={onClick}   
          />

  );

export default TextareaField;

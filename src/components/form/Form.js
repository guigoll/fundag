import React, {Component } from 'react';
import { Form } from 'react-bootstrap';

const FormLogin = ({
   onSubmit,
   onChange,
   ...props,
}) =>(
      <div className="form-box">
        <Form name="form" onSubmit={onSubmit} className={props.className}>
           {props.children}
        </Form>
      </div>
);
export default FormLogin;

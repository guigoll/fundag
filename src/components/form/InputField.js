import React, {Component} from 'react';
import { Icon } from 'react-icons-kit'
import {photo} from 'react-icons-kit/fa/photo'

const InputField = ({
  value,
  name,
  type,
  className,
  id,
  placeholder,
  onChange,
  onClick,
}) =>(
    <input
      value = { value}
      className = {className}
      id= {id}
      name = {name}
      type = {type}
      placeholder = {placeholder}
      onChange = {onChange}
      onClick = {onClick}    
      />
);

export default InputField;

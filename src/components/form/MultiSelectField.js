import React, {Component} from 'react';

const MultiSelectField = ({
  value,
  type,
  options,
  id,
  onChange,
  ...props,
}) => (
  <div>
     <select
        name="description[]"
        value={value}
        className="multiselect multiselect-custom"
        id="multiSelect"
        onChange={onChange}
        multiple="multiple" 
        data-parsley-required data-parsley-trigger-after-failure="change" 
        data-parsley-errors-container="#error-multiselect"
        >       
        {
          options.map((option, index) =>(
            <option
                key={index}
                value={option} >
                  {option}
            </option>
          ))}
      </select>
      <p id="error-multiselect"></p>
  </div>
);

export default MultiSelectField;

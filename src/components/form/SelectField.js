import React, {Component} from 'react';

const SelectField = ({
  value,
  name,
  type,
  options,
  className,
  id,
  onChange,
  ...props,
}) => (
  <div>
     <select
        name= {name}
        value={value}
        className={className}
        id={id}
        onChange={onChange}
        >
        <option value="0">Selecione {props.valor}</option>
        {
          options.map((option, index) =>(
            <option
                key={index}
                value={option} >
                  {option}
            </option>
          ))}
      </select>
  </div>
);

export default SelectField;

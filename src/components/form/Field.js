import React, {Component} from 'react';

const onHandleChange =(validate, onChange) => event => {
  const{ value} = event.target;
  const isValid = validate(value);

  onChange(value, isValid);
}

const getStyle = isValid => {
   border: isValid? '': '1px solid red';
}

const Field = ({
  component: Component,
  validate,
  isValid,
  onChange,
  onClick,
  ...props,
}) => (
      <Component
          style={getStyle(isValid)}
          onChange={onHandleChange(validate, onChange)}
          onClick={onClick}
          {...props}
      />
);

export default Field;

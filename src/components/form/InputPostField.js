import React, {Component} from 'react';
import { Icon } from 'react-icons-kit'
import {photo} from 'react-icons-kit/fa/photo'

const InputPostField = ({
  value,
  name,
  type,
  className,
  id,
  placeholder,
  onChange,
  onClick,
}) =>(
  <div className="input-group">
    <input
      value = { value}
      className = {className}
      id= {id}
      name = {name}
      type = {type}
      placeholder = {placeholder}
      onChange = {onChange}
      onClick = {onClick}
      />
    <div className="input-group-prepend">
        <a href="#">
          <Icon icon={photo} size={24} />
        </a>
    </div>
  </div>
);

export default InputPostField;

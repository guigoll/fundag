import React, { Component } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css'

const DataGrid = ({data ,
  columns,
  ...props,
}) => ({
  render() {

    const Title = props.title

    return (
      <div className="row">
            <div className="panel-content">
            <hr />
          <ReactTable
            data={data}
            columns={columns}
            defaultPageSize={5}
            className="-striped -highlight"
            previousText='Anterior'
            nextText='Proxímo'
            loadingText='Carregando...'
            noDataText='Nenhum registro encontrado'
            pageTex='Páginas'
            ofText='de'
            rowsText='Linhas'
          />
        </div>
      </div>
    )
  }
});

export default DataGrid;

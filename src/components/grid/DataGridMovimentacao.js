import React, { Component } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { Form, SelectField } from '../form';

class DataGridMovimentacao extends Component {
    render() {
        const data = []
        const columns = [{
            Header: 'Nº',
            accessor: 'codigo'
        }, {
            Header: 'Data',
            accessor: 'data',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Forma Pagamento',
            accessor: 'formaPag',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Localidade',
            accessor: 'localidade',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Tipo Despesa',
            accessor: 'tipoDesp',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Nota Fiscal',
            accessor: 'notaFiscal',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Valor Lcto',
            accessor: 'valorLcto',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Moeda Lcto',
            accessor: 'moedaLcto',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Valor',
            accessor: 'valor',
            Cell: props => <span className='text-left'>{props.value}</span>
        }, {
            Header: 'Moeda',
            accessor: 'moeda',
            Cell: props => <span className='text-left'>{props.value}</span>
        }]

        return (
            <div className="row">
                <div className="panel-content">
                <hr />
                    <ReactTable
                        data={data}
                        columns={columns}
                        defaultPageSize={5}
                        className="-striped -highlight"
                        previousText='Anterior'
                        nextText='Proxímo'
                        loadingText='Carregando...'
                        noDataText='Relatorio vazio'
                        pageTex='Páginas'
                        ofText='de'
                        rowsText='Linhas'
                    />
                </div>
            </div >
        )
    }
};

export default DataGridMovimentacao;

import React, { Component } from 'react';
import { Form, SelectField } from '../form';

const TableGrid = ({
    className,
    ...props,
}) => (
        <table className={className}>
            {props.children}
        </table>
    )


const TableHeader = ({ headers }) => (
    <thead>
        {
            headers.map((node, i) => {
                <th key={i}>
                    {node.description}
                </th>
            })
        }
    </thead>
)

const TableLine = ({ props }) => ({
    render() {
        const { description, p1, p2, p3, p4 } = this.props.linhas;
        return (
            <tbody>
                <tr>
                    <td>{this.description}</td>
                    <td>{this.p1}</td>
                    <td>{this.p2}</td>
                    <td>{this.p3}</td>
                    <td>{this.p4}</td>
                </tr>
            </tbody>
        )
    }
});





class DataGridPlanoDeTrebalho extends Component {

    renderTreeView(_array) {
        if (_array !== undefined) {
            return (
                _array.map((node, i) => {
                    return (
                        <TableLine
                            description={node.description}
                            p1={node.p1}
                            p2={node.p2}
                            p3={node.p3}
                            p4={node.p4}
                        >
                            {this.renderTreeView(node.children)}
                        </TableLine>
                    )
                })
            )
        }
    }
    render() {

        const headers = [{
            description: 'Descrição',
        }, {
            description: '1',
        }, {
            description: '2',
        }, {
            description: '3',
        }, {
            description: '4',
        }]

        const linhaSup = [{
            description: 'Investimento',
            p1: '100,00',
            p2: '200,00',
            p3: '50,00',
            p4: '10,00',
            children: [{
                description: 'Custeio',
                p1: '100,00',
                p2: '200,00',
                p3: '50,00',
                p4: '10,00'
            }]
        }]

        return (
            <div>
                <Form onSubmit={this.onSubmit}>
                    <div className="row">
                        <div className="col-md-3">
                            <div className="panel-content">
                                <div className="form-group">
                                    <SelectField
                                        className="form-control right"
                                        options={['Mensal', 'Semanal', 'Bimestral', 'Anual ']}
                                        valor="um periodo"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </Form>
                <div className="panel-group">
                    <div className="panel panel-default">
                        <table className="table table-extrato ">
                            <thead>
                                <tr>
                                    <td>Descrição</td>
                                    <td>1</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td>4</td>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" href="#combinedOne" className="collapse in">
                                    <table className="table table-extrato">
                                        <thead>
                                            <tr>
                                                <td>Investimento</td>
                                                <td>100,00</td>
                                                <td>200,00</td>
                                                <td>50,00</td>
                                                <td>10,00</td>
                                            </tr>
                                        </thead>
                                    </table>
                                </a>
                            </h4>
                        </div>
                        <div id="combinedOne" className="panel-collapse collapse in">
                            <table className="table table-striped">
                                <tbody className="table-extrato">
                                    <tr>
                                        <td>Compra Licenças</td>
                                        <td>100,00</td>
                                        <td>200,00</td>
                                        <td>50,00</td>
                                        <td>10,00</td>
                                    </tr>
                                    <tr>
                                        <td>Compra Laptop</td>
                                        <td>30,00</td>
                                        <td>100,00</td>
                                        <td>25,00</td>
                                        <td>200</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="panel panel-danger">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" href="#combinedTwo" className="collapsed">
                                    <table className="table table-extrato">
                                        <thead>
                                            <tr>
                                                <td>Custeio</td>
                                                <td>100,00</td>
                                                <td>200,00</td>
                                                <td>50,00</td>
                                                <td>10,00</td>
                                            </tr>
                                        </thead>
                                    </table>
                                </a>
                            </h4>
                        </div>
                        <div id="combinedTwo" className="panel-collapse collapse">
                            <table className="table table-extrato">
                                <tbody>
                                    <tr>
                                        <td>Combustível, Pedágio</td>
                                        <td>50,00</td>
                                        <td></td>
                                        <td></td>
                                        <td>100,00</td>
                                    </tr>
                                    <tr>
                                        <td>Diárias</td>
                                        <td>25,00</td>
                                        <td></td>
                                        <td></td>
                                        <td>100,00</td>

                                    </tr>
                                    <tr>
                                        <td>Custo Laboratorio</td>
                                        <td>25,00</td>
                                        <td>50,00</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Diversos</td>
                                        <td>25,00</td>
                                        <td></td>
                                        <td>25,00</td>
                                        <td>100,00</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div >
            </div>

        )
    }
}

export default DataGridPlanoDeTrebalho;
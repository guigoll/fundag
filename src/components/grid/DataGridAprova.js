import React, { Component } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';


import { ButtonDropdow } from '../button';

const DataGridAprova = ({
  columns,
  data,
  ...props,
}) => ({
  render() {


    const Title = props.title

    return (
      <div className="row">
            <div className="panel-content">
            <hr />
          <ReactTable
            data={data}
            columns={columns}
            defaultPageSize={5}
            previousText='Anterior'
            nextText='Proxímo'
            loadingText='Carregando...'
            noDataText='Nenhum registro encontrado'
            pageTex='Páginas'
            ofText='de'
            rowsText='Linhas'
            className="-striped -highlight"
          />
          </div> 
      </div>
    )
  }
});

export default DataGridAprova;

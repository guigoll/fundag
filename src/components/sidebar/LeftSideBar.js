import React, { Component}  from 'react';

const LeftSideBar = (props) => (
  <div id="left-sidebar" className="sidebar">
      <button type="button" className="btn btn-xs btn-link btn-toggle-fullwidth">
        <span className="sr-only">Toggle Fullwidth</span>
        <i className="fa fa-angle-left"></i>
      </button>
      <div className="sidebar-scroll">
       {props.children}
     </div>
  </div>
);

export default LeftSideBar;

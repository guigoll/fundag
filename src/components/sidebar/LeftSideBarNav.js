import React, { Component } from 'react'
import { Link } from 'react-router-dom';

const LeftSideBarNav = () => (
  <nav id="left-sidebar-nav" className="sidebar-nav">
    <ul id="main-menu" className="metismenu">
      <li>
        <Link to="/home">
          <i className="lnr lnr-home"></i>
          <span>Dashboard</span>
        </Link>
      </li>
      <li className="active">
        <a href="#subPages" className="has-arrow" aria-expanded="false">
          <i className="lnr lnr-pencil"></i>
          <span>
            <strong>Solicitações</strong>
          </span>
        </a>
        <ul aria-expanded="true">
          <li>
            <Link to="/solicitacoesprojeto">Exibir Solicitacoes</Link>
          </li>
          <li>
            <Link to="/adiantamento">Adiantamento</Link>
          </li>
          <li>
            <Link to="/home">Diária</Link>
          </li>
          <li>
            <Link to="/home">Quilometragem</Link>
          </li>
          <li>
            <Link to="/reembolso">Reembolso</Link>
          </li>
          <li>
            <Link to="/home">Ordem de Pagamento</Link>
          </li>
        </ul>
      </li>
      <li>
        <Link to='/projetos'>
          <i className="lnr lnr-magic-wand"></i>
          <span>Projeto</span>
        </Link>
      </li>
      <li>
        <Link to='/extrato'>
          <i className="lnr lnr-map"></i>
          <span>Extrato</span>
        </Link>
      </li>   
      <li>
        <a href="#forms" className="has-arrow" aria-expanded="false">
          <i className="lnr lnr-book"></i>
          <span>Cadastros Básicos</span>
        </a>
        <ul aria-expanded="true">
          <li>
            <a href="page-profile.html">Funcionários</a>
          </li>
          <li>
            <Link to="/centrodecustos">Centro de Custo</Link>
          </li>
          <li>
            <Link to="/contascontabeis">Conta Contábil</Link>
          </li>
        </ul>
      </li>
      <li>
        <a href="#">
          <i className="lnr lnr-briefcase"></i>
          <span>Política Reembolso</span>
        </a>
      </li>
    </ul>
  </nav>
);

export default LeftSideBarNav;

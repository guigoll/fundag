import React, {Component} from 'react'

const SideBarScroll = (props) => (
  <div className="sidebar-scroll">
      {props.children}
  </div>
)

export default SideBarScroll;

import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';


const ModalGenerico = ({
    handleShow,
    show,
    onHide,
    dialogClassName,
    handleClose,
    Component,
    ...props
}) => ({
    render() {
        return (
            <Modal
                show={show}
                onHide={handleClose}
                dialogClassName={dialogClassName}
            >    

                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-lg">{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {Component}
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={handleClose}>Fechar</Button>
                </Modal.Footer>
            </Modal>
        );
    }
});

export default ModalGenerico;

import React, { Component} from 'react';
import { Modal,Button, ButtonToolbar } from 'react-bootstrap';

class ExibeModal extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      show: false
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {
    return (
      <div className="pull-right">
          <button onClick={this.handleShow} className={this.props.btnClass}>
               <i className={this.props.btnIcon}></i>
              {this.props.btnDesc}
          </button>

          <Modal
               show={this.state.show}
               onHide={this.handleClose}
               dialogClassName="modal-lg"
               >

            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-lg">{this.props.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                 {this.props.body}
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.handleClose}>Fechar</Button>
            </Modal.Footer>
          </Modal>
       </div>
    );
  }
}

export default ExibeModal;

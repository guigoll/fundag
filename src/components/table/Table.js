import _ from 'lodash';
import React, { Component } from 'react';

class Table extends Component {
  render() {
    const { columns, data } = this.props;   
    let [names, props] = _.zip(...columns);
    let headers = <tr>{names.map((name, n) => <th key={n}>{name}</th>)}</tr>
    let rows = data.map((item, i) => <tr key={i}>{props.map((prop, p) => <td key={p}>{item[prop]}</td>)}</tr>);

    return (
      <table className="table">
        <thead>{headers}</thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
};

export default Table;
import React, { Component}  from 'react';

const NavBarMenu = (props) => (
<div id="navbar-menu">
  <ul className="nav navbar-nav">
    <li className="dropdown">
      <a href="#" className="dropdown-toggle icon-menu" data-toggle="dropdown">
        <i className="lnr lnr-alarm"></i>
        <span className="notification-dot"></span>
      </a>
      <ul className="dropdown-menu notifications">
        <li className="header"><strong>Você tem 7 novas notificações</strong></li>
        <li>
          <a href="#">
            <div className="media">
              <div className="media-left">
                <i className="fa fa-fw fa-flag-checkered text-muted"></i>
              </div>
              <div className="media-body">
                <p className="text">Os últimos projetos ficaram todos dentro da política de margem esperada. Parabéns!</p>
                <span className="timestamp">24 minutos atrás</span>
              </div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="media">
              <div className="media-left">
                <i className="fa fa-fw fa-exclamation-triangle text-warning"></i>
              </div>
              <div className="media-body">
                <p className="text">Seu budget do projeto <strong>Fazenda São Miguel</strong> atingiu 70%. Favor verificar.</p>
                <span className="timestamp">2 horas atrás</span>
              </div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="media">
              <div className="media-left">
                <i className="fa fa-fw fa-bar-chart text-muted"></i>
              </div>
              <div className="media-body">
                <p className="text">Suas despesas estão 27% maiores que semana passada.</p>
                <span className="timestamp">Ontem</span>
              </div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="media">
              <div className="media-left">
                <i className="fa fa-fw fa-check-circle text-success"></i>
              </div>
              <div className="media-body">
                <p className="text">Seu orçamento <strong>Visita Feira BH</strong> foi aprovado. Parabéns!</p>
                <span className="timestamp">2 dias atrás</span>
              </div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="media">
              <div className="media-left">
                <i className="fa fa-fw fa-exclamation-circle text-danger"></i>
              </div>
              <div className="media-body">Seu relatório
                     <p><strong>Visita Sertãozinho</strong> foi reprovado por estar fora da política. Favor revisar.</p>
                <span className="timestamp">3 dias atrás</span>
              </div>
            </div>
          </a>
        </li>
        <li className="footer">
          <a href="#" className="more">Veja todas as notificações</a>
       </li>
      </ul>
    </li>
    <li className="dropdown">
      <a href="#" className="dropdown-toggle icon-menu" data-toggle="dropdown">
        <i className="lnr lnr-cog"></i>
      </a>
      <ul className="dropdown-menu user-menu menu-icon">
      <li className="menu-heading">CONFIGURAÇÕES</li>
        <li><a href="#"><i className="fa fa-fw fa-edit"></i> <span>Básicas</span></a></li>
        <li><a href="#"><i className="fa fa-fw fa-bell"></i> <span>Notificações</span></a></li>
        <li><a href="#"><i className="fa fa-fw fa-sliders"></i> <span>Preferências</span></a></li>
        <li><a href="#"><i className="fa fa-fw fa-lock"></i> <span>Privacidade</span></a></li>
        <li className="menu-heading">COBRANÇA</li>
        <li><a href="#"><i className="fa fa-fw fa-file-text-o"></i> <span>Faturas</span></a></li>
        <li><a href="#"><i className="fa fa-fw fa-credit-card"></i> <span>Pagamentos</span></a></li>
        <li><a href="#"><i className="fa fa-fw fa-refresh"></i> <span>Renovações</span></a></li>
        <li className="menu-button">
          <a href="#" className="btn btn-primary"><i className="fa fa-rocket"></i> UPGRADE </a>
        </li>
      </ul>
    </li>
    <li className="dropdown">
      <a href="#" className="dropdown-toggle icon-menu" data-toggle="dropdown">
        <i className="lnr lnr-question-circle"></i>
      </a>
      <ul className="dropdown-menu user-menu">
        <li>
          <form className="search-form help-search-form">
            <input value="" className="form-control" placeholder="Como posso ajudar?" type="text" />
            <button type="button" className="btn btn-default"><i className="fa fa-search"></i></button>
          </form>
        </li>
        <li className="menu-heading">COMO</li>
        <li><a href="#">Cadastrar um Projeto</a></li>
        <li><a href="#">Cadastrar um C.Custo</a></li>
        <li><a href="#">Cadastrar uma C.Contábil</a></li>
        <li><a href="#">Cadastrar um Orçamento </a></li>
        <li className="menu-heading">CONTA</li>
        <li><a href="page-profile.html">Alterar Senha</a></li>
        <li><a href="#">Preferências</a></li>
        <li className="menu-button">
          <a href="#" className="btn btn-primary"><i className="fa fa-question-circle"></i> HELP CENTER</a>
        </li>
      </ul>
    </li>
  </ul>
</div>
);


export default NavBarMenu;

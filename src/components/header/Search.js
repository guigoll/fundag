import React, { Component}  from 'react';

const Search = (props) => (
  <form id="navbar-search" className="navbar-form search-form">
    <input value="" className="form-control" placeholder="Pesquisar..." type="text" />
    <button type="button" className="btn btn-default"><i className="fa fa-search"></i></button>
  </form>
);

export default Search;

import React, { Component}  from 'react';

const Navbar = (props) => (
  <nav className="navbar navbar-default navbar-fixed-top">
     <div className="container-fluid">
       <div className="navbar-btn">
         <button type="button" className="btn-toggle-offcanvas"><i className="lnr lnr-menu"></i></button>
       </div>
       <div className="navbar-brand">
         <a href="index.html"><img src="images/logo.png" alt="FUNDAG" className="img-responsive logo" /></a>
       </div>
       <div className="navbar-right">
              { props.children}
       </div>
     </div>
   </nav>
);

export default Navbar;

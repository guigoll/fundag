import React, { Component}  from 'react' ;

const LoginContainer = (props)=> (
  <div className="container">
    <div className="login-container">
      {props.children}
    </div>
  </div>
);

export default LoginContainer;

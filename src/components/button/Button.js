import React, { Component } from 'react';

const Button = ({
    className,
    type,
    ...props
}) => (
        <button
            className={className}
            type={type}>
            <i className={props.icon}></i>
            {props.Text}
        </button>
    );

export default Button;

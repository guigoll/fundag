import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';

class ButtonDropdow extends Component {
    constructor() {
        super();
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.state = {
            addItem: false,
            show: false,
        }
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }
    render() {
        return (
            <div className="btn-group dropup">
                <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    Ação <span className="caret"></span>
                </button>
                <ul className="dropdown-menu" role="menu">
                    {console.log(this.props.options)}
                    {this.props.options.map((option, index) => (
                        <li key={index}>

                            <a onClick={this.handleShow}>
                                <i className={option.icon}></i>
                                {option.description}
                            </a>

                            <Modal
                                show={this.state.show}
                                onHide={this.handleClose}
                                dialogClassName="modal-lg"
                            >

                                <Modal.Header closeButton>
                                    <Modal.Title id="contained-modal-title-lg">{option.title}</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button onClick={this.handleClose}>Fechar</Button>
                                </Modal.Footer>
                            </Modal>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

export default ButtonDropdow;
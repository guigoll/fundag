import React, {Component} from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Perfil } from '../../screens';


const PrivateSubRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
       <Perfil>
          <Component/>
      </Perfil>
   )} />
)

export default PrivateSubRoute;

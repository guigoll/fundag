import React, { Component } from 'react'

const MainContent = (props) => (
  <div id="main-content">
    <div className="container-fluid">
      {props.children}
    </div>
  </div>
);

export default MainContent;

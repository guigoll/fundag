import React, { Component } from 'react';
import AnyChart from 'anychart-react';

const PieChart = ({
  width,
  height,
  titileChart,
  ...props,
}) => (
  <div className="panel-content">
    <AnyChart
      id="pieChart"
      width={500}
      height={500}
      type="pie"
      data={[1, 2, 3, 4]}
      title={titileChart}
    />
  </div>
);
export default PieChart;



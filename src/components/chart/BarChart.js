import React, { Component } from 'react';
import AnyChart from 'anychart-react';

const BarChart = ({
  width,
  height,
  titileChart,
  ...props,
}) => (
  <div className="panel-content">
    <AnyChart
      id="columnChart"
      width={500}
      height={500}
      type="column"
      data={"P1,5\nP2,3\nP3,6\nP4,4"}
      title={titileChart}
    />
  </div>
);
export default BarChart;



import React, { Component } from 'react';
import { Form, InputField, SelectField } from '../form';
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend
} from 'recharts';

const data = [
    { name: '02/18', uv: 4000, pv: 2400, amt: 2400 },
    { name: '03/18', uv: 3000, pv: 1398, amt: 2210 },
    { name: '04/18', uv: 2000, pv: 9800, amt: 2290 },
    { name: '05/18', uv: 2780, pv: 3908, amt: 2000 },
    { name: '06/18', uv: 1890, pv: 4800, amt: 2181 },
    { name: '07/18', uv: 2390, pv: 3800, amt: 2500 },
    { name: '08/18', uv: 3490, pv: 4300, amt: 2100 },
    { name: '09/18', uv: 3490, pv: 4300, amt: 2100 },
    { name: '10/18', uv: 3490, pv: 4300, amt: 2100 },
    { name: '11/18', uv: 3490, pv: 4300, amt: 2100 },
    { name: '12/18', uv: 3490, pv: 4300, amt: 2100 },
];

class sLineChart extends Component {
    render() {
        return (
            <div>
                <Form onSubmit={this.onSubmit}>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="panel-content">
                                <div className="form-group">
                                    <SelectField
                                        className="form-control right"
                                        options={['Projeto 1', 'Projeto 2']}
                                        valor="um projeto"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </Form>

                <LineChart width={500} height={400} data={data}
                    margin={{ top: 5, right: 5, left: 5, bottom: 5 }}>
                    <XAxis dataKey="name" />
                    <YAxis />
                    <CartesianGrid strokeDasharray="3 3" />
                    <Tooltip />
                    <Legend />
                    <Line type="monotone" dataKey="pv" stroke="#8884d8" activeDot={{ r: 8 }} />
                    <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
                </LineChart>
            </div>

        );
    }
}

export default sLineChart;

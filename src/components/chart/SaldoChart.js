
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const SaldoChart = () => (
    <div className="col-md-3 col-sm-3">
        <div className="panel-content">
            <table className="table no-margin">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Nº Doc.</th>
                        <th>Historico</th>
                        <th>Debito</th>
                        <th>Credito</th>           
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>21/01/2018</td>
                        <td>00000000000000</td>
                        <td>Despesas KM</td>
                        <td>0</td>
                        <td>10000,00</td>                    
                    </tr>
                    <tr>
                        <td>21/01/2018</td>
                        <td>00000000000000</td>
                        <td>Despesas Diária</td>
                        <td>300,00</td>
                        <td>1000,00</td>                   
                    </tr>                    
                    <tr>
                        <td></td>
                        <td>
                        <strong>Saldo</strong> 1000,00</td>                       
                    </tr>
                </tbody>
            </table>
        </div>
        <div className="action-buttons right">
            <Link to="/projetoDetalhe" className="btn btn-default">
                <i className="fa fa-file-text-o"></i>
                Visualizar detalhes do projeto
            </Link>
        </div>
    </div>
);


export default SaldoChart;
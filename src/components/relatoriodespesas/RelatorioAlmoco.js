import React, { Component } from 'react';
import { Form, InputField, TextareaField } from '../form';


class RelatorioAlmoco extends Component {
    render() {
        return (
            <Form onSubmit={this.onSubmit}>
                <div className="row">
                    <div className="col-md-6">
                        <div className="panel-content">
                            <div className="form-group">
                                <div className="row">
                                    <div className="col-md-3">
                                        <label>Valor</label>
                                        <InputField
                                            type="text"
                                            name="valor"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="col-md-3">
                                        <label>Nº Pessoas</label>
                                        <InputField
                                            type="text"
                                            name="nPessoas"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <label>Justificativa/Observações</label>
                                        <TextareaField
                                            type="text"
                                            name="observacao"
                                            rows="4"
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <label>Nota Fiscal</label>
                                        <InputField
                                            type="text"
                                            name="notafiscal"
                                            className="form-control"
                                            onChange={this.onChange}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Form >
        );
    }
};

export default RelatorioAlmoco;
import React, { Component } from 'react';

class RelatorioBody extends Component {
    constructor() {
        super();
        this.state = {
            addItem: false,
        }
    }

    addItem() {
        var Visible;
        if (this.state.addItem) {
            Visible = false
        } else {
            Visible = true
        }

        console.log(Visible)
        this.setState({
            addItem: Visible
        })
    }
    render() {
        return (
            <div className="row">
                <div className="panel-content">
                    <Form onSubmit={this.onSubmit}>
                        <div className="form-group col-md-3">
                            <SelectField
                                value="1"
                                className="form-control"
                                valor="colaborador"
                                options={['Guilherme', 'Marcelo']}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="form-group col-md-3">
                            <SelectField
                                value="1"
                                className="form-control"
                                valor="Tipo de Relatorio"
                                options={['Cartão', 'Viagem Nacional', 'Despesas']}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="form-group col-md-3">
                            <SelectField
                                value="1"
                                className="form-control"
                                valor="um projeto"
                                options={['Projeto 1', 'Projeto 2', 'Projeto 3']}
                                onChange={this.onChange}
                            />
                        </div>

                    </Form>
                    <button onClick={this.addItem.bind(this)} className="btn btn-primary">Novo</button>
                </div>
            </div>
        );
    }
}

export default RelatorioBody;
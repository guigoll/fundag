import Relatorio from './Relatorio';
import RelatorioBody from './RelatorioBody';

export {
    Relatorio,
    RelatorioBody,
}
import React, { Component } from 'react';
import { Form, InputField, TextareaField } from '../form';

class RelatorioHospedagem extends Component {
    render() {
        return (
            <Form onSubmit={this.onSubmit}>
                <div className="row">
                    <div className="panel-content">
                        <div className="form-group">
                            <div className="row">
                                <div className="col-md-3">
                                    <label>Valor</label>
                                    <InputField
                                        type="text"
                                        name="valor"
                                        className="form-control"
                                        onChange={this.onChange}
                                    />
                                </div>
                                <div className="col-md-3">
                                    <label>Data Inicial</label>
                                    <InputField
                                        type="text"
                                        name="nInicial"
                                        className="form-control"
                                        onChange={this.onChange}
                                    />
                                </div>
                                <div className="col-md-3">
                                    <label>Data Final</label>
                                    <TextareaField
                                        type="text"
                                        name="dtFinal"
                                        rows="4"
                                        className="form-control"
                                    />
                                </div>
                                <div className="col-md-3">
                                    <label>Numero Diárias</label>
                                    <InputField
                                        type="text"
                                        name="nDiarias"
                                        className="form-control"
                                        onChange={this.onChange}
                                    />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group">
                                    <label>Justificativa/Observações</label>
                                    <TextareaField
                                        type="text"
                                        name="observacao"
                                        rows="4"
                                        className="form-control"
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Nota Fiscal</label>
                                    <InputField
                                        type="text"
                                        name="notafiscal"
                                        className="form-control"
                                        onChange={this.onChange}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Form >
        );
    }
}

export default RelatorioHospedagem;
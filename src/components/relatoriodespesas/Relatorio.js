import React, { Component } from 'react';
import { Form, SelectField, InputField } from '../form';
import { DataGridMovimentacao } from '../grid';

class Relatorio extends Component {
    constructor() {
        super();
        this.state = {
            addItem: false,
        }
    }

    addItem() {
        var Visible;
        if (this.state.addItem) {
            Visible = false
        } else {
            Visible = true
        }

        console.log(Visible)
        this.setState({
            addItem: Visible
        })
    }
    render() {
        const { addItem } = this.state;
        return (
            <div>
                <div className="row">
                    <div className="panel-content">
                        <Form onSubmit={this.onSubmit}>
                            <div className="form-group col-md-3">
                                <SelectField
                                    value="1"
                                    className="form-control"
                                    valor="colaborador"
                                    options={['Guilherme', 'Marcelo']}
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="form-group col-md-3">
                                <SelectField
                                    value="1"
                                    className="form-control"
                                    valor="Tipo de Relatorio"
                                    options={['Cartão', 'Viagem Nacional', 'Despesas']}
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="form-group col-md-3">
                                <SelectField
                                    value="1"
                                    className="form-control"
                                    valor="um projeto"
                                    options={['Projeto 1', 'Projeto 2', 'Projeto 3']}
                                    onChange={this.onChange}
                                />
                            </div>

                        </Form>
                        <button onClick={this.addItem.bind(this)} className="btn btn-primary">Novo</button>
                    </div>
                </div>
                {addItem ?
                    <Form onSubmit={this.onSubmit}>
                        <div className="panel-group">
                            {/* Renderizar o primeiro header */}
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h4 className="panel-title">
                                        <a data-toggle="collapse" href="#combinedOne" className="collapse in">
                                            <table className="table table-striped">
                                                <thead className="table-item">
                                                    <tr>
                                                        <td><strong>Adiantamento</strong></td>
                                                        <td><strong>Devolução</strong></td>
                                                        <td><strong>Reembolsavél</strong></td>
                                                        <td><strong>Não reembolsável</strong></td>
                                                        <td><strong>Total</strong></td>
                                                        <td><strong>Saldo à reembolsar</strong></td>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </a>
                                    </h4>
                                </div>
                                <div id="combinedOne" className="panel-collapse collapse in">
                                    <table className="table table-striped">
                                        <tbody className="table-item">
                                            <tr>
                                                <td>0,00</td>
                                                <td>0,00</td>
                                                <td>0,00</td>
                                                <td>0,00</td>
                                                <td>0,00</td>
                                                <td>0,00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/* Renderizar o segundo header */}
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h4 className="panel-title">
                                        <a data-toggle="collapse" href="#combinedTwo" className="collapse in">
                                            <table className="table table-striped">
                                                <thead className="table-item">
                                                    <tr>
                                                        <td><strong>Código</strong> </td>
                                                        <td><strong>Tipo</strong></td>
                                                        <td><strong>Colaborador</strong></td>
                                                        <td><strong>Moeda</strong></td>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </a>
                                    </h4>
                                </div>
                                <div id="combinedTwo" className="panel-collapse collapse in">
                                    <table className="table table-striped">
                                        <tbody className="table-item">
                                            <tr>
                                                <td>1011</td>
                                                <td>Cartão Corporativo</td>
                                                <td>Administrador</td>
                                                <td>Real</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/* dispara o change para renderizar o componente relativo a despesa */}
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h4 className="panel-title">
                                        <a data-toggle="collapse" href="#combinedTree" className="collapse in">
                                            <table className="table table-striped">
                                                <thead className="table-item">
                                                    <tr>
                                                        <td><strong>Data Despesa</strong> </td>
                                                        <td><strong>Forma de Pagamento</strong></td>
                                                        <td><strong>Moeda</strong></td>
                                                        <td><strong>Localidade</strong></td>
                                                        <td><strong>Tipo de Despesa</strong></td>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </a>
                                    </h4>
                                </div>
                                <div id="combinedTree" className="panel-collapse collapse in">
                                    <table className="table table-striped">
                                        <tbody className="table-item">
                                            <tr>
                                                <td>
                                                    <InputField
                                                        type="text"
                                                        name="dataDespesa"
                                                        className="form-control"
                                                        onChange={this.onChange}
                                                    />
                                                </td>
                                                <td>
                                                    <SelectField
                                                        value="1"
                                                        className="form-control"
                                                        valor="uma forma de pagemento"
                                                        options={['Cartão', 'Viagem Nacional', 'Despesas']}
                                                        onChange={this.onChange}
                                                    />
                                                </td>
                                                <td>
                                                    <SelectField
                                                        value="1"
                                                        className="form-control"
                                                        valor="uma moeda"
                                                        options={['Cartão', 'Viagem Nacional', 'Despesas']}
                                                        onChange={this.onChange}
                                                    />
                                                </td>
                                                <td>
                                                    <SelectField
                                                        value="1"
                                                        className="form-control"
                                                        valor="uma localidade"
                                                        options={['Campinas', 'Americana', 'São Paulo']}
                                                        onChange={this.onChange}
                                                    />
                                                </td>
                                                <td>
                                                    <SelectField
                                                        value="1"
                                                        className="form-control"
                                                        valor="Tipo de despesa"
                                                        options={['Km', 'Diaria', 'Almoço']}
                                                        onChange={this.onChange}
                                                    />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            {/* Renderizar o componente com tipo da despesas selecionado */}

                            {/* Adicionar no grid a despesa selecionado */}
                            <DataGridMovimentacao />
                        </div>
                    </Form>
                    : null}
            </div>
        );
    }
}

export default Relatorio;
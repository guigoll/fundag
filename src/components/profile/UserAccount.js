import React, {Component} from 'react'

const UserAccount = (props) => (
  <div className="user-account">
    { props.children}
    <div className="dropdown">
       <a href="#" className="dropdown-toggle user-name" data-toggle="dropdown">Olá,
         <strong>FUNDAG</strong>
      <em className="fa fa-caret-down"></em>
    </a>
    <ul className="dropdown-menu dropdown-menu-right account">
        <li><a href="#"><strong>Empresa: </strong>Matriz</a></li>
        <li className="divider"></li>
        <li><a href="page-profile.html"><strong>Perfil: </strong>Administrador</a></li>
        <li><a href="#"><strong>C.Custo: </strong>Adm 1005976</a></li>
        <li className="divider"></li>
        <li><a href="#">Informações do Sistema</a></li>
        <li className="divider"></li>
        <li><a href="#">Logout</a></li>
       </ul>
    </div>
  </div>
);

export default UserAccount;

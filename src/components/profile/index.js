import UserAccount from './UserAccount';
import UserPhoto from './UserPhoto';

export {
  UserAccount,
   UserPhoto
}

import React, { Component}  from 'react';

const UserPhoto = (props) => (
    <img src="images/perfil/user.png" className="img-responsive img-circle user-photo" alt="User Profile Picture" />
)

export default UserPhoto;

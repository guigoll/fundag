import React, { Component } from 'react';

const Section = (props) => (
    <div className="dashboard-section">
        <div className="section-heading clearfix">
            <h2 className="section-title"><em className={props.icon}></em> {props.titulo}</h2>
        </div>
        <div className="row">
            <div className={props.col} >
                <div className="panel-content">
                    <h3 className="heading"><em className={"fa fa-square"}></em> {props.subtitulo}</h3>
                    {props.children}
                </div>
            </div>
        </div>
    </div>
);

export default Section;

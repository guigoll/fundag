import React, { Component } from 'react';

const SectionGenerico = ({
    options,
    ...props
}) => (
        <div className="dashboard-section">
            <div className="section-heading clearfix">
                <h2 className="section-title"><em className={props.icon}></em> {props.titulo}</h2>
            </div>
            <div className="row">
                {
                    options.map((option, index) => (
                        <div className={option.col}  key={index}>
                            <div className="panel-content">
                                <h3 className="heading"><em className={"fa fa-square"}></em> {option.subtitulo}</h3>
                                {option.component}
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    );

export default SectionGenerico;

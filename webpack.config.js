
module.exports = {
  entry: './src/index.js',
  output: {
    path: __dirname + '/public',
    filename: './bundle.js'
  },
  devServer: {
    port: 8080,
    contentBase: './public',
    historyApiFallback: true,
  },
  performance: { hints: false },

  module: {
    rules: [{
      test: /.js?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'react', 'stage-3']
      }
    },
    {
      test: /\.css$/,
      use: ['style-loader', 'css-loader']
    },
    {
      test: /\.(jpg|jpeg|gif|png)$/,
      exclude: /node_modules/,
      use: [
        'url-loader?limit=1024&name=images/[name].[ext]'
      ]
    },
    {
      test: /\.(woff|woff2|eot|ttf|svg)$/,
      use: ['file-loader?name=src/fonts/[name].[ext]']
    }]
  }
};
